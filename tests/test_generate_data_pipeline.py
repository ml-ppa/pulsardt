import pytest
import numpy as np

from src.pulsar_simulation.generate_data_pipeline import GenData, run_gen_data_in_parallel
from src.pulsar_simulation.pulsar_animator import PulsarAnimator


# Initialize the GenData instance with a mock PulsarAnimator
@pytest.fixture
def gen_data():
    return GenData(pulsar_animator=PulsarAnimator(rot_axis=[1, 0, 0], mag_axis_tilt=45))


# Test __call__ method for data generation
def test_gen_data_call(gen_data):
    freq_channels = np.arange(0.5, 1.6, 0.1).tolist()
    rot_phases = np.arange(0, 360, 1).tolist()
    
    # Run the method
    gendata_obj = GenData(pulsar_animator=PulsarAnimator(rot_axis=[1, 0, 0], mag_axis_tilt=45))
    output=gendata_obj(tag='test_0',
                        rot_phases=rot_phases,
                        freq_channels=freq_channels,
                        param_folder='./params/runtime/',
                        payload_folder='./params/payloads/')

    # Check output type
    assert isinstance(output, tuple), "Output should be a tuple."
    assert len(output) == 2, "Output tuple should have two elements."
    assert isinstance(output[0], np.ndarray), "First element should be a numpy array."
    assert isinstance(output[1], np.ndarray), "Second element should be a numpy array."
    
    # Check the dimensions of the outputs
    assert output[0].shape == (len(rot_phases), len(freq_channels)), "Noisy output dimensions are incorrect."
    assert output[1].shape == (len(rot_phases), len(freq_channels)), "Flux output dimensions are incorrect."

# Test that the parameters are stored correctly upon initialization
def test_gen_data_initialization(gen_data):
    assert gen_data.param_folder == [], "Initial parameter folder should be an empty list."
    assert isinstance(gen_data.pulsar_animator, PulsarAnimator), "Pulsar animator should be a MockPulsarAnimator instance."