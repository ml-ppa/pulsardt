import pytest
import numpy as np

from src.pulsar_simulation.signal_detection import antenna
from src.pulsar_simulation.information_packet_formats import Payload

@pytest.fixture
def sample_payload():
    payload = Payload(freqs=np.linspace(1, 10, 10))
    payload.dataframe = np.random.random((10, 10))
    payload.rot_phases = np.linspace(0, 360, 10)
    return payload

@pytest.fixture
def test_antenna():
    return antenna(sensitivity=0.5, prob_bbrfi=0.5, prob_nbrfi=0.5)

# Test __init__ method
def test_antenna_initialization():
    ant = antenna(sensitivity=0.5, prob_bbrfi=0.8, prob_nbrfi=0.3)
    assert ant.sensitivity == 0.5
    assert ant.prob_bbrfi == 0.8
    assert ant.prob_nbrfi == 0.3
    assert ant.freq_channels == []

# Test __call__ method
def test_antenna_call(test_antenna, sample_payload):
    output = test_antenna(sample_payload)
    assert isinstance(output, tuple) and len(output) == 3, "Output should be a tuple of length 3"
    assert isinstance(output[0], np.ndarray), "First element should be an ndarray (noisy output)"
    assert isinstance(output[1], np.ndarray), "Second element should be an ndarray (total flux)"
    assert isinstance(output[2], dict), "Third element should be a dict (description)"

# Test randomize_antenna_output_method
def test_randomize_antenna_output_method(test_antenna, sample_payload):
    noisy_output, total_flux, description = test_antenna.randomize_antenna_output_method(sample_payload)
    assert noisy_output.shape == sample_payload.dataframe.shape, "Noisy output shape mismatch"
    assert total_flux.shape == sample_payload.dataframe.shape, "Total flux shape mismatch"
    assert 'Pulsar' in description, "Description should include 'Pulsar' key"

# Test is_pulsar_present
def test_is_pulsar_present_with_signal(test_antenna, sample_payload):
    test_antenna.dataframe = np.random.uniform(0.5, 1.0, (10, 10))
    pulsar_present = test_antenna.is_pulsar_present()
    assert pulsar_present == 1, "Expected pulsar to be present with flux above sensitivity"

def test_is_pulsar_present_without_signal(test_antenna, sample_payload):
    test_antenna.dataframe = np.random.uniform(0.0, 0.4, (10, 10))
    pulsar_present = test_antenna.is_pulsar_present()
    assert pulsar_present == 0, "Expected no pulsar with flux below sensitivity"

# Test gen_terresterial_noise
def test_gen_terresterial_noise(test_antenna, sample_payload):
    test_antenna.extract_payload_info(sample_payload)
    total_noise, description = test_antenna.gen_terresterial_noise(
        effected_freq_channel_by_nbrfi=sample_payload.freqs[5],
        nbrfi_bandwidth=0.01,
        effected_rot_phase_by_bbrfi=sample_payload.rot_phases[5],
        bbrfi_duration=10,
        noise_amplitude_range=[3, 10]
    )
    assert total_noise.shape == sample_payload.dataframe.shape, "Noise output shape mismatch"
    assert 'NBRFI' in description and 'BBRFI' in description, "Description keys missing"

# Test extract_payload_info
def test_extract_payload_info(test_antenna, sample_payload):
    test_antenna.extract_payload_info(sample_payload)
    assert np.array_equal(test_antenna.freq_channels, sample_payload.freqs), "Frequency channels not set correctly"
    assert np.array_equal(test_antenna.rot_phases, sample_payload.rot_phases), "Rotation phases not set correctly"
    assert np.array_equal(test_antenna.dataframe, sample_payload.dataframe), "Dataframe not set correctly"

# Test gen_noise_NBRFI
def test_gen_noise_NBRFI(test_antenna, sample_payload):
    test_antenna.extract_payload_info(sample_payload)
    nbrfi_noise = test_antenna.gen_noise_NBRFI(
        effected_freq_channel=sample_payload.freqs[5],
        nbrfi_amplitude=5,
        spread_in_channels=0.5
    )
    assert nbrfi_noise.shape == sample_payload.dataframe.shape, "NBRFI noise shape mismatch"
    assert np.all(nbrfi_noise >= 0), "NBRFI noise should have non-negative values"

# Test gen_noise_BBRFI
def test_gen_noise_BBRFI(test_antenna, sample_payload):
    test_antenna.extract_payload_info(sample_payload)
    bbrfi_noise = test_antenna.gen_noise_BBRFI(
        effected_rot_phase=sample_payload.rot_phases[5],
        bbrfi_amplitude=5,
        spread_in_phase=10
    )
    assert bbrfi_noise.shape == sample_payload.dataframe.shape, "BBRFI noise shape mismatch"
    assert np.all(bbrfi_noise >= 0), "BBRFI noise should have non-negative values"

# Test antennaTemperature
def test_antenna_temperature(test_antenna):
    temperature = test_antenna.antennaTemperature(receivedFlux=5)
    assert temperature == 5, "Temperature should equal received flux in default function"

# Test calculateAntennaOutputVoltage
def test_calculate_antenna_output_voltage(test_antenna):
    voltage = test_antenna.calculateAntennaOutputVoltage(received_flux=5)
    assert isinstance(voltage, float), "Voltage output should be a float"