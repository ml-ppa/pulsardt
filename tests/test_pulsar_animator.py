import pytest
import numpy as np
from src.pulsar_simulation.pulsar_animator import PulsarAnimator
from src.pulsar_simulation.spark_pattern_generator import (
    SparkPatternGenerator,
    SparkSpectralModel,
)


def test_pulsar_animator_initialization():
    # Sample input for initialization
    rot_axis = [0.0, 0.0, 1.0]
    mag_axis_tilt = 45.0
    period = 33.0
    
    # Initialize PulsarAnimator object
    pulsar_animator = PulsarAnimator(rot_axis=rot_axis, mag_axis_tilt=mag_axis_tilt, period=period)
    
    # Check that attributes are correctly initialized
    assert pulsar_animator._PulsarAnimator__rot_axis == rot_axis, "Rotation axis did not initialize correctly"
    assert pulsar_animator._PulsarAnimator__mag_axis_tilt == mag_axis_tilt, "Magnetic axis tilt did not initialize correctly"
    assert pulsar_animator.period == period, "Period did not initialize correctly"
    
    # Check that additional attributes are initialized as None or empty lists
    assert pulsar_animator.mag_frame_basis_att0 is None, "mag_frame_basis_att0 should be None"
    assert pulsar_animator.rot_frame_basis is None, "rot_frame_basis should be None"
    assert pulsar_animator.mag_frame_basis is None, "mag_frame_basis should be None"
    assert pulsar_animator.spark_genlist == [], "spark_genlist should be an empty list"
    assert pulsar_animator.spark_spectrum_model_list == [], "spark_spectrum_model_list should be an empty list"


@pytest.mark.parametrize("rot_axis, mag_axis_tilt, period", [
    ([1.0, 0.0, 0.0], 30.0, 50.0),
    ([0.0, 1.0, 0.0], 60.0, 100.0),
    ([0.0, 0.0, 1.0], 90.0, 33.0)
])
def test_pulsar_animator_different_inputs(rot_axis, mag_axis_tilt, period):
    # Initialize with parameterized inputs
    pulsar_animator = PulsarAnimator(rot_axis=rot_axis, mag_axis_tilt=mag_axis_tilt, period=period)
    
    # Validate inputs
    assert pulsar_animator._PulsarAnimator__rot_axis == rot_axis, f"Expected rot_axis {rot_axis}"
    assert pulsar_animator._PulsarAnimator__mag_axis_tilt == mag_axis_tilt, f"Expected mag_axis_tilt {mag_axis_tilt}"
    assert pulsar_animator.period == period, f"Expected period {period}"


def test_pulsar_animator_default_period():
    # Initialize without specifying period
    pulsar_animator = PulsarAnimator(rot_axis=[0.0, 1.0, 0.0], mag_axis_tilt=30.0)
    
    # Validate default period
    assert pulsar_animator.period == 33, "Default period should be 33 ms"

def test_add_spark_single():
    # Create an instance of PulsarAnimator
    pulsar_animator = PulsarAnimator(rot_axis=[0.0, 1.0, 0.0], mag_axis_tilt=45.0)

    # Create a spark generator and add it with the default spectral model
    spark_gen = SparkPatternGenerator(0)
    pulsar_animator.add_spark(spark_gen)

    # Verify that the spark_genlist and spark_spectrum_model_list each have one item
    assert len(pulsar_animator.spark_genlist) == 1, "spark_genlist should have 1 item"
    assert len(pulsar_animator.spark_spectrum_model_list) == 1, "spark_spectrum_model_list should have 1 item"

    # Check that the added objects are correct
    assert pulsar_animator.spark_genlist[0] == spark_gen, "Expected spark generator in list"
    assert isinstance(pulsar_animator.spark_spectrum_model_list[0], SparkSpectralModel), \
        "Expected default SparkSpectralModel instance"


def test_add_spark_custom_spectral_model():
    pulsar_animator = PulsarAnimator(rot_axis=[0.0, 1.0, 0.0], mag_axis_tilt=45.0)

    # Create spark generator and custom spectral model
    spark_gen = SparkPatternGenerator(0)
    custom_spectral_model = SparkSpectralModel()  # Customize if parameters are available
    pulsar_animator.add_spark(spark_gen, custom_spectral_model)

    # Verify that the spark_genlist and spark_spectrum_model_list each have one item
    assert len(pulsar_animator.spark_genlist) == 1, "spark_genlist should have 1 item"
    assert len(pulsar_animator.spark_spectrum_model_list) == 1, "spark_spectrum_model_list should have 1 item"

    # Check the correct objects are in the lists
    assert pulsar_animator.spark_genlist[0] == spark_gen, "Expected spark generator in list"
    assert pulsar_animator.spark_spectrum_model_list[0] == custom_spectral_model, "Expected custom spectral model"


def test_add_multiple_sparks():
    pulsar_animator = PulsarAnimator(rot_axis=[0.0, 1.0, 0.0], mag_axis_tilt=45.0)

    spark_gen1 = SparkPatternGenerator(0)
    spark_gen2 = SparkPatternGenerator(1)
    spectral_model1 = SparkSpectralModel()
    spectral_model2 = SparkSpectralModel()

    # Add multiple sparks
    pulsar_animator.add_spark(spark_gen1, spectral_model1)
    pulsar_animator.add_spark(spark_gen2, spectral_model2)

    # Verify the lists contain two items each
    assert len(pulsar_animator.spark_genlist) == 2, "spark_genlist should have 2 items"
    assert len(pulsar_animator.spark_spectrum_model_list) == 2, "spark_spectrum_model_list should have 2 items"

    # Check that the correct objects are in the lists
    assert pulsar_animator.spark_genlist[0] == spark_gen1, "First spark generator should match"
    assert pulsar_animator.spark_genlist[1] == spark_gen2, "Second spark generator should match"
    assert pulsar_animator.spark_spectrum_model_list[0] == spectral_model1, "First spectral model should match"
    assert pulsar_animator.spark_spectrum_model_list[1] == spectral_model2, "Second spectral model should match"


def test_empty_spark_list_initially():
    # Ensure lists are empty upon initialization
    pulsar_animator = PulsarAnimator(rot_axis=[0.0, 1.0, 0.0], mag_axis_tilt=45.0)
    assert pulsar_animator.spark_genlist == [], "spark_genlist should be empty initially"
    assert pulsar_animator.spark_spectrum_model_list == [], "spark_spectrum_model_list should be empty initially"

# Fixtures for common test data
@pytest.fixture
def default_rot_axis():
    return [0.0, 0.0, 1.0]  # example axis pointing in the Z direction

@pytest.fixture
def mag_axis_tilt():
    return 45.0  # example tilt in degrees

@pytest.fixture
def pulsar_animator(default_rot_axis, mag_axis_tilt):
    return PulsarAnimator(rot_axis=default_rot_axis, mag_axis_tilt=mag_axis_tilt)


# Test Cases
def test_update_pulsar_state_vectors_zero_phase(pulsar_animator):
    """Test update_pulsar_state_vectors with a rot_phase of 0 degrees"""
    pulsar_animator.update_pulsar_state_vectors(rot_phase=0)

    # Check that mag_frame_basis_att0 and mag_frame_basis are correctly set
    assert pulsar_animator.mag_frame_basis_att0 is not None, "mag_frame_basis_att0 should be initialized"
    assert pulsar_animator.mag_frame_basis is not None, "mag_frame_basis should be initialized"

    # Check that mag_frame_basis is equal to mag_frame_basis_att0 for zero rotation
    for vec1, vec2 in zip(pulsar_animator.mag_frame_basis, pulsar_animator.mag_frame_basis_att0):
        np.testing.assert_almost_equal(vec1, vec2, decimal=6, err_msg="mag_frame_basis should match mag_frame_basis_att0 with zero rotation")


def test_update_pulsar_state_vectors_random_phase(pulsar_animator):
    """Test update_pulsar_state_vectors with a random rot_phase value"""
    rot_phase = 33.0
    pulsar_animator.update_pulsar_state_vectors(rot_phase=rot_phase)

    # Check that the basis vectors are initialized
    assert pulsar_animator.rot_frame_basis is not None, "rot_frame_basis should be initialized"
    assert pulsar_animator.mag_frame_basis_att0 is not None, "mag_frame_basis_att0 should be initialized"
    assert pulsar_animator.mag_frame_basis is not None, "mag_frame_basis should be initialized"

    # Verify that rotation produces valid vectors (unit length)
    for basis_vector in pulsar_animator.mag_frame_basis:
        np.testing.assert_almost_equal(
            np.linalg.norm(basis_vector), 1.0, decimal=6, err_msg="mag_frame_basis vectors should have unit length"
        )


def test_update_pulsar_state_vectors_mag_axis_tilt(pulsar_animator, mag_axis_tilt):
    """Test update_pulsar_state_vectors with the effect of magnetic axis tilt"""
    pulsar_animator.update_pulsar_state_vectors(rot_phase=0)

    # Calculate the expected tilt in radians
    tilt_rad = mag_axis_tilt * np.pi / 180

    # Verify that the Y-axis component of mag_frame_basis_att0 has been tilted by mag_axis_tilt
    np.testing.assert_almost_equal(
        np.dot(pulsar_animator.mag_frame_basis_att0[2], pulsar_animator.rot_frame_basis[2]),
        np.cos(tilt_rad),
        decimal=6,
        err_msg="mag_frame_basis_att0 should have been rotated by mag_axis_tilt about the Y-axis"
    )