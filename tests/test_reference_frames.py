import pytest
import numpy as np
from src.pulsar_simulation.reference_frames import calculate_geodesic_distance, rotate_reference_frame, polar2cart

# Tests for calculate_geodesic_distance
def test_calculate_geodesic_distance_same_points():
    """Test geodesic distance between two identical points"""
    point = [1.0, 0.0, 0.0]
    distance = calculate_geodesic_distance(point, point)
    assert distance == 0, "Distance should be zero for identical points"

def test_calculate_geodesic_distance_orthogonal_points():
    """Test geodesic distance between two orthogonal points on unit sphere"""
    point_0 = [1.0, 0.0, 0.0]
    point_1 = [0.0, 1.0, 0.0]
    distance = calculate_geodesic_distance(point_0, point_1)
    assert np.isclose(distance, np.pi / 2), "Distance should be pi/2 for orthogonal points"

def test_calculate_geodesic_distance_antipodal_points():
    """Test geodesic distance between two opposite points on unit sphere"""
    point_0 = [1.0, 0.0, 0.0]
    point_1 = [-1.0, 0.0, 0.0]
    distance = calculate_geodesic_distance(point_0, point_1)
    assert np.isclose(distance, np.pi), "Distance should be pi for antipodal points"

# Tests for rotate_reference_frame
def test_rotate_reference_frame_90_degrees():
    """Test rotating a basis around an axis by 90 degrees"""
    from scipy.spatial.transform import Rotation as R

    frame_0_basis = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
    axis_of_rotation = [0, 0, 1]  # Rotate around z-axis
    rotation_angle = np.pi / 2  # 90 degrees

    rotated_basis = rotate_reference_frame(frame_0_basis, axis_of_rotation, rotation_angle)
    expected_basis = [[0, 1, 0], [-1, 0, 0], [0, 0, 1]]  # Expected rotated basis

    for rotated_vec, expected_vec in zip(rotated_basis, expected_basis):
        assert np.allclose(rotated_vec, expected_vec), f"Rotated vector {rotated_vec} does not match expected {expected_vec}"

def test_rotate_reference_frame_zero_rotation():
    """Test rotating a basis with a zero rotation angle (should return the original basis)"""
    frame_0_basis = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
    axis_of_rotation = [0, 0, 1]
    rotation_angle = 0  # Zero rotation

    rotated_basis = rotate_reference_frame(frame_0_basis, axis_of_rotation, rotation_angle)

    for original_vec, rotated_vec in zip(frame_0_basis, rotated_basis):
        assert np.allclose(original_vec, rotated_vec), f"Rotated vector {rotated_vec} should match original {original_vec}"

def test_rotate_reference_frame_180_degrees():
    """Test rotating a basis by 180 degrees around an axis"""
    frame_0_basis = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
    axis_of_rotation = [1, 0, 0]  # Rotate around x-axis
    rotation_angle = np.pi  # 180 degrees

    rotated_basis = rotate_reference_frame(frame_0_basis, axis_of_rotation, rotation_angle)
    expected_basis = [[1, 0, 0], [0, -1, 0], [0, 0, -1]]

    for rotated_vec, expected_vec in zip(rotated_basis, expected_basis):
        assert np.allclose(rotated_vec, expected_vec), f"Rotated vector {rotated_vec} does not match expected {expected_vec}"

# Tests for polar2cart
def test_polar2cart_origin():
    """Test polar to cartesian conversion at origin"""
    result = polar2cart(0, 0, 0)
    expected = [0, 0, 0]
    assert result == expected, f"Expected {expected} but got {result}"

def test_polar2cart_positive_r():
    """Test polar to cartesian conversion for r=1, theta=pi/2, phi=0 (should be on x-axis)"""
    r = 1
    theta = np.pi / 2
    phi = 0
    result = polar2cart(r, theta, phi)
    expected = [1, 0, 0]
    assert np.allclose(result, expected), f"Expected {expected} but got {result}"

def test_polar2cart_z_axis():
    """Test polar to cartesian conversion for point on positive z-axis"""
    r = 1
    theta = 0  # Polar angle is zero (z-axis)
    phi = 0
    result = polar2cart(r, theta, phi)
    expected = [0, 0, 1]
    assert np.allclose(result, expected), f"Expected {expected} but got {result}"

def test_polar2cart_y_axis():
    """Test polar to cartesian conversion for point on positive y-axis"""
    r = 1
    theta = np.pi / 2  # 90 degrees
    phi = np.pi / 2  # 90 degrees
    result = polar2cart(r, theta, phi)
    expected = [0, 1, 0]
    assert np.allclose(result, expected), f"Expected {expected} but got {result}"