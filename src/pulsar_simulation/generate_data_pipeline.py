from os import chdir
import sys
import numpy as np
import json
import ray
import time
from tqdm import tqdm
import matplotlib.pyplot as plt


from .defines import *
from .file_operations import file_operations as fo
from .information_packet_formats import Payload
from .pulsar_animator import PulsarAnimator
from .spark_pattern_generator import SparkPatternGenerator,SparkSpectralModel
from .radio_packet_propagation import InterstellarMedium
from .signal_detection import antenna
from .remote_functions import run_gen_data_method


class GenData:
    def __init__(self,
                 pulsar_animator:PulsarAnimator,
                 ): #cannot be in more than 1 place
        self.pulsar_animator = pulsar_animator
        self.param_folder = []
        
        

    def __call__(self,
                 freq_channels:list[float]=None,
                 rot_phases:list[float]=None,
                 tag:str='_test_0',
                 signal_time_bin:float=33.0/360,
                 param_folder:str='./params/runtime/',
                 payload_folder:str='../params/payloads/',
                 antenna_sensitivity:float = 0.5,
                 prob_nbrfi:float=0.5,
                 prob_bbrfi:float=0.5,
                 scale_direction_randomness:float=0.5,
                 remove_drift_effect:bool=True
                 ):
        self.param_folder=param_folder
        self.tag = tag
        self.freq_channels = freq_channels
        self.rot_phases = rot_phases
        #: Create the spark pattern
        spark_list,spark_spectrum_model_list = self.generate_random_spark_pattern(num_cones=avg_spark_pattern_params.num_cones,
                                                        avg_spark_dimension=avg_spark_pattern_params.avg_spark_dimension,
                                                        avg_spark_per_cone_length=avg_spark_pattern_params.avg_spark_per_cone_length,
                                                        avg_spark_pattern_center_tilt=avg_spark_pattern_params.avg_spark_pattern_center_tilt,
                                                        avg_drift_vel=avg_spark_pattern_params.avg_drift_vel)
        self.pulsar_animator.spark_genlist=[]
        self.pulsar_animator.spark_genlist = spark_list
        self.pulsar_animator.spark_spectrum_model_list = spark_spectrum_model_list
        self.pulsar_animator(rot_phase=0)
        #: Choose a random direction and rotate the pulsar to generate a radio packet to be propagated      
        direction = self.generate_random_direction(scale_direction_randomness=scale_direction_randomness)
        payload_obj = self.pulsar_animator.generate_radio_packet_stream(rot_phases=rot_phases,direction=direction)        
        payload_obj.write_payload_to_jsonfile(file_name=payload_folder + self.tag + '_payload_raw.json')
        payload_to_be_propagated = self.generate_payload_at_aquisition_frequency(payload_raw=payload_obj)
        payload_to_be_propagated.write_payload_to_jsonfile(file_name=payload_folder + self.tag + '_payload_tobe_propagated.json')

        random_ism_obj = self.generate_random_ism_obj()
        payload_propagated = random_ism_obj.propagate_through(payload=payload_to_be_propagated,signal_time_bin=signal_time_bin,average_over_period=remove_drift_effect)
        


        #: Save the propagated payload after antenna detection
        antenna_obj = antenna(sensitivity=antenna_sensitivity,prob_bbrfi=prob_bbrfi,prob_nbrfi=prob_nbrfi)
        total_noisy_output,total_flux,description = antenna_obj(payload=payload_propagated )
        payload_detected = Payload(freqs=payload_propagated.freqs)
        payload_detected.dataframe = np.array(total_noisy_output).tolist()
        payload_detected.assign_rot_phases(rot_phases=payload_propagated.rot_phases)
        payload_detected.add_description(description=description)
        payload_detected.write_payload_to_jsonfile(file_name=payload_folder + self.tag + '_payload_detected.json')
        

        payload_flux = Payload(freqs=payload_propagated.freqs)
        payload_flux.dataframe = np.array(total_flux).tolist()
        payload_flux.assign_rot_phases(rot_phases=payload_propagated.rot_phases)
        payload_flux.add_description(description=description)
        payload_flux.write_payload_to_jsonfile(file_name=payload_folder + self.tag + '_payload_flux.json')
        
        #print('dataframe.shape',dataframe.shape)
        return total_noisy_output,total_flux
    
    def plot(self,
                 freq_channels:list[float]=None,
                 rot_phases:list[float]=None,
                 tag:str='_test_0',
                 signal_time_bin:float=33.0/360,
                 param_folder:str='./params/runtime/',
                 payload_folder:str='../params/payloads/',
                 antenna_sensitivity:float = 0.5,
                 prob_nbrfi:float=0.5,
                 prob_bbrfi:float=0.5,
                 scale_direction_randomness:float=0.5,
                 remove_drift_effect:bool = True
                 ):
        total_noisy_output,total_flux = self.__call__(freq_channels=freq_channels,
                                                      rot_phases=rot_phases,
                                                      tag=tag,
                                                      signal_time_bin=signal_time_bin,
                                                      param_folder=param_folder,
                                                      payload_folder=payload_folder,
                                                      prob_nbrfi=prob_nbrfi,
                                                      prob_bbrfi=prob_bbrfi,
                                                      antenna_sensitivity=antenna_sensitivity,
                                                      scale_direction_randomness=scale_direction_randomness,
                                                      remove_drift_effect=remove_drift_effect
                                                      )
        fig1, ax = plt.subplots(1, 2, figsize=(10, 5))
        # dist_output = ConnectedComponents.skeletonize_image(dispersed_freq_time_segmented=dispersed_freq_time_segmented)
        ax[0].imshow(total_noisy_output.T)
        ax[0].set_xlabel("phase")
        ax[0].set_ylabel("freq channel")
        ax[0].grid("True")
        ax[0].set_aspect("auto")
        #ax[0].invert_yaxis()       
        ax[1].imshow(total_flux.T)
        ax[1].set_xlabel("phase")
        ax[1].set_ylabel("freq channel")
        ax[1].grid("True")
        ax[1].set_aspect("auto")
        #ax[1].invert_yaxis()    
        
        
    
    def generate_random_spark_pattern(self,num_cones:int,
                                      avg_spark_dimension:float,
                                      avg_spark_per_cone_length:int,
                                      avg_spark_pattern_center_tilt:float,
                                      avg_drift_vel:float):        
        spark_dimension = np.max([0,np.random.normal(loc=avg_spark_dimension,scale=avg_spark_dimension/4)])
        spark_dimension_radian = spark_dimension*np.pi/180        
        spark_per_cone_length = np.max([1,np.random.normal(loc=avg_spark_per_cone_length,scale=avg_spark_per_cone_length/4)])        
        conal_latitudes = [0.01+i*spark_dimension*2 for i in range(num_cones)]        
        conal_circumference = [2*np.pi*np.cos(c_l*np.pi/180) for c_l in conal_latitudes]
        num_sparks = [np.ceil(c_c*spark_per_cone_length) for c_c in conal_circumference]
        #spark_pattern_center_tilt = np.floor(avg_spark_pattern_center_tilt*np.random.random())
        random_phase = np.random.random()*360 
        spark_pattern_center_tilt = (avg_spark_pattern_center_tilt)       
        spark_rotation_axis_polar_att0 = [spark_pattern_center_tilt,random_phase]
        drift_vel = np.random.normal(loc=avg_drift_vel,scale=avg_drift_vel/4)
        spark_mid_freqs = [0.50*(i+1) for i in range(num_cones)]
        spark_flux_powers = [10*1/(0+1) for i in range(num_cones)]        
        spark_list = SparkPatternGenerator.create_patterened_spark_pattern(num_sparks=num_sparks,
                                                                           conal_latitudes=conal_latitudes,
                                                                           spark_rotation_axis_polar_att0=spark_rotation_axis_polar_att0,
                                                                           drift_vel=drift_vel,
                                                                           spark_dimension=spark_dimension_radian,
                                                                           spark_mid_freqs=spark_mid_freqs,
                                                                           spark_flux_powers=spark_flux_powers)
        spark_spectrum_model_list = [SparkSpectralModel() for sp in spark_list]
        param_selected_dict_for_sparks = {'num_sparks':num_sparks,
                               'conal_latitudes':conal_latitudes,
                               'spark_rotation_axis_polar_att0':spark_rotation_axis_polar_att0,
                               'drift_vel':drift_vel,
                               'spark_dimension':spark_dimension_radian,
                               'spark_mid_freqs':spark_mid_freqs,
                               'num_sparks':num_sparks,
                               'spark_flux_powers':spark_flux_powers}
        filename = self.param_folder + self.tag + '.json' # This needs to be changed
        fo.append_to_json_file(file_path=filename,new_data=param_selected_dict_for_sparks)   
        return spark_list,spark_spectrum_model_list
    
    def generate_random_direction(self,scale_direction_randomness:float=0.5,):
        direction= self.pulsar_animator.mag_frame_basis_att0[2]
        #print('debug',direction)        
        direction = np.array([np.random.normal(loc=direction[0],scale=scale_direction_randomness),np.random.normal(loc=direction[1],scale=scale_direction_randomness),np.random.normal(loc=direction[2],scale=scale_direction_randomness)])
        #print('debug',direction)
        direction = (direction/np.linalg.norm(direction)).tolist()
        #print('debug',direction)
        filename = self.param_folder + self.tag + '.json' 
        param_selected_dict_for_direction = {'direction':direction}
        fo.append_to_json_file(file_path=filename,new_data=param_selected_dict_for_direction)
        return direction
    
    def generate_random_ism_obj(self):
        avg_ism_obj = InterstellarMedium(dm_homogeneous=avg_ism_params.avg_dm_homogeneous,
                                         scintillation_index_homo=avg_ism_params.std_scintillation_index,
                                         std_dm=avg_ism_params.std_dm,
                                         std_scintillation_index=avg_ism_params.std_scintillation_index)
        avg_ism_obj.randomize_props()
        filename = self.param_folder + self.tag + '.json' 
        param_selected_dict_for_ism = avg_ism_obj.__dict__
        fo.append_to_json_file(file_path=filename,new_data=param_selected_dict_for_ism)
        return avg_ism_obj


    
    def generate_payload_at_aquisition_frequency(self,payload_raw:Payload):
        spark_spectrum_models = self.pulsar_animator.spark_spectrum_model_list        
        spark_mid_freq = payload_raw.freqs
        freq_channels = np.array(self.freq_channels)
        relative_flux_mat = np.array([s_m(center_freq=s_m_f,freq=freq_channels) for s_m,s_m_f in zip(spark_spectrum_models,spark_mid_freq)]).T 
        raw_dataframe = np.array(payload_raw.dataframe).T        
        dataframe = np.matmul(relative_flux_mat,raw_dataframe)
        payload_to_be_propagated = Payload(freqs=freq_channels.tolist())
        payload_to_be_propagated.dataframe = (dataframe.T).tolist()
        payload_to_be_propagated.assign_rot_phases(rot_phases=payload_raw.rot_phases) 
        return payload_to_be_propagated
    
def run_gen_data_in_parallel(gendata_obj:GenData,
                             tag_list:list[str],
                             rot_phases=np.arange(0,720*1,1).tolist(),
                             freq_channels=np.arange(0.5,1.6,0.001),
                             antenna_sensitivity:float = 0.5,
                             prob_nbrfi:float=0.5,
                             prob_bbrfi:float=0.5,
                             scale_direction_randomness:float=0.5,
                             num_cpus:int=10,
                             return_execution_time:bool=True,
                             reinit_ray:bool=True,
                             remove_drift_effect:bool =True,
                             param_folder='./params/runtime/',
                             payload_folder:str='../params/payloads/'):
                             
    
    if ray.is_initialized() and reinit_ray:
        ray.shutdown()
        ray.init(num_cpus=num_cpus)
    

    freq_channels_shared = ray.put(freq_channels)
    rot_phases_shared = ray.put(rot_phases)
    payload_folder_shared = ray.put(payload_folder) 
    param_folder_shared = ray.put(param_folder)
    antenna_sensitivity_shared = ray.put(antenna_sensitivity)
    prob_nbrfi_shared=ray.put(prob_nbrfi)
    prob_bbrfi_shared=ray.put(prob_bbrfi)
    scale_direction_randomness_shared:float=ray.put(scale_direction_randomness)
    remove_drift_effect_shared = ray.put(remove_drift_effect)
    
    if return_execution_time:strt_time = time.time()
    ray.get([run_gen_data_method.options(scheduling_strategy="SPREAD").remote(func=gendata_obj,tag=tg,
                                                 rot_phases=rot_phases_shared,
                                                 freq_channels=freq_channels_shared,
                                                 antenna_sensitivity = antenna_sensitivity_shared,
                                                 prob_nbrfi = prob_nbrfi_shared,
                                                 prob_bbrfi = prob_bbrfi_shared,
                                                 scale_direction_randomness = scale_direction_randomness_shared,
                                                 remove_drift_effect = remove_drift_effect_shared,
                                                 payload_folder=payload_folder_shared,
                                                 param_folder=param_folder_shared) for tg in tag_list])

    if return_execution_time: return time.time()-strt_time

def generate_randomized_data_payloads(
        gendata_obj:GenData,
        tag:str,
        num_payloads:int,
        rot_phases=np.arange(0,720*1,1).tolist(),
        freq_channels=np.arange(0.5,1.6,0.001),
        antenna_sensitivity:float = 0.5,
        prob_nbrfi:float=0.5,
        prob_bbrfi:float=0.5,
        scale_direction_randomness:float=0.5,
        remove_drift_effect:bool =True,
        num_cpus:int=10,               
        param_folder='./params/runtime/',
        payload_folder:str='../params/payloads/',
        reinit_ray:bool=True,
        tag_index_offset:int=0
        ):
    
    if ray.is_initialized() and reinit_ray:
        ray.shutdown()
        ray.init(num_cpus=num_cpus)

    tags = [tag+str(x+tag_index_offset) for x in range(num_payloads)] 
    distributed_tag_list = [tags[x*num_cpus:(x+1)*num_cpus] for x in range(np.ceil(len(tags)/num_cpus).astype(int))]
    for tag_list in tqdm(distributed_tag_list):
        run_gen_data_in_parallel(gendata_obj=gendata_obj,
                         rot_phases=rot_phases,
                         antenna_sensitivity=antenna_sensitivity,
                         freq_channels=freq_channels,
                         tag_list=tag_list,
                         param_folder=param_folder,
                         payload_folder=payload_folder,
                         reinit_ray=False,
                         return_execution_time=False,
                         prob_bbrfi=prob_bbrfi,
                         prob_nbrfi=prob_nbrfi,
                         scale_direction_randomness=scale_direction_randomness,
                         remove_drift_effect=remove_drift_effect)


def generate_example_payloads_for_training( tag:str='train_v0_',
                                            num_payloads:int=10,
                                            rot_phases=np.arange(0,360*2,5).tolist(),
                                            freq_channels=np.arange(0.25,.85,0.001),
                                            antenna_sensitivity:float = 0.5,
                                            prob_nbrfi:float=0.5,
                                            prob_bbrfi:float=0.5,
                                            scale_direction_randomness:float=0.5,
                                            remove_drift_effect:bool =True,
                                            num_cpus:int=10,               
                                            param_folder='./params/runtime/',
                                            payload_folder:str='./params/payloads/',
                                            reinit_ray:bool=True,
                                            tag_index_offset:int=0,
                                            plot_a_example:bool=True,                                            
                                            ):
    pulsar_animator = PulsarAnimator(rot_axis=[0,0,1],mag_axis_tilt=45)
    pulsar_animator(rot_phase=0)
    gendata_obj = GenData(pulsar_animator=pulsar_animator)
    rot_phases =  rot_phases
    freq_channels = freq_channels
    if plot_a_example:
        gendata_obj.plot(tag=tag,
                rot_phases=rot_phases,
                freq_channels=freq_channels,
                param_folder=param_folder,
                payload_folder=payload_folder,
                antenna_sensitivity=antenna_sensitivity,
                prob_bbrfi=prob_bbrfi,
                prob_nbrfi=prob_nbrfi,
                scale_direction_randomness=scale_direction_randomness,
                remove_drift_effect=remove_drift_effect
                )
    generate_randomized_data_payloads(gendata_obj=gendata_obj,
                         rot_phases=rot_phases,
                         freq_channels=freq_channels,
                         tag=tag,
                         num_payloads=num_payloads,
                         param_folder=param_folder,payload_folder=payload_folder,
                         prob_bbrfi=prob_bbrfi,
                         prob_nbrfi=prob_nbrfi,
                         antenna_sensitivity=antenna_sensitivity,
                         tag_index_offset=tag_index_offset,
                         num_cpus=num_cpus,
                         reinit_ray=reinit_ray)
    


        