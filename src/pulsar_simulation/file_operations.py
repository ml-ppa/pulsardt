import json
filename:str= '..\\params\\runtime\\test_0.json'
param_selected_dict = {'new0':0}

class file_operations:
    def read_json_file(file_path):
        try:
            with open(file_path, 'r') as file:
                data = json.load(file)
                return data
        except FileNotFoundError:
            return {}
        except json.JSONDecodeError:
            return {}


    def write_json_file(file_path, data):
        with open(file_path, 'w') as file:
            json.dump(data, file, indent=4)


    def append_to_json_file(file_path, new_data):
        data = file_operations.read_json_file(file_path)
        if isinstance(data, dict):
            data.update(new_data)
        else:
            raise ValueError("JSON file does not contain a list at the root")
        file_operations.write_json_file(file_path, data)
