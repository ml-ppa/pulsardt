import os
#import sys
#sys.path.append("../")
import numpy as np
import math as mt

num_cpus = os.cpu_count()
numTaskpCPU = 1
import ray


#: Define here the unbound methods to be called in the classes below for parallelization
@ray.remote(num_cpus=num_cpus)
def rotate_vector_in3D(
    vectors: list, rot_x_rad: float = 0, rot_y_rad: float = 0, rot_z_rad: float = 0
):
    rot_mat = [
        [
            mt.cos(rot_y_rad) * mt.cos(rot_z_rad),
            (
                mt.sin(rot_x_rad) * mt.sin(rot_y_rad) * mt.cos(rot_z_rad)
                - mt.cos(rot_x_rad) * mt.sin(rot_z_rad)
            ),
            (
                mt.cos(rot_x_rad) * mt.sin(rot_y_rad) * mt.cos(rot_z_rad)
                + mt.sin(rot_x_rad) * mt.sin(rot_z_rad)
            ),
        ],
        [
            mt.cos(rot_y_rad) * mt.sin(rot_z_rad),
            (
                mt.sin(rot_x_rad) * mt.sin(rot_y_rad) * mt.sin(rot_z_rad)
                + mt.cos(rot_x_rad) * mt.cos(rot_z_rad)
            ),
            (
                mt.cos(rot_x_rad) * mt.sin(rot_y_rad) * mt.sin(rot_z_rad)
                - mt.sin(rot_x_rad) * mt.cos(rot_z_rad)
            ),
        ],
        [
            -mt.sin(rot_y_rad),
            mt.sin(rot_x_rad) * mt.cos(rot_y_rad),
            mt.cos(rot_x_rad) * mt.cos(rot_y_rad),
        ],
    ]
    rotated_vector_x = [
        (
            rot_mat[0][0] * vector[0]
            + rot_mat[0][1] * vector[1]
            + rot_mat[0][2] * vector[2]
        )
        for vector in vectors
    ]
    rotated_vector_y = [
        (
            rot_mat[1][0] * vector[0]
            + rot_mat[1][1] * vector[1]
            + rot_mat[1][2] * vector[2]
        )
        for vector in vectors
    ]
    rotated_vector_z = [
        (
            rot_mat[2][0] * vector[0]
            + rot_mat[2][1] * vector[1]
            + rot_mat[2][2] * vector[2]
        )
        for vector in vectors
    ]

    rotated_vector = [
        [rotated_vector_x[i], rotated_vector_y[i], rotated_vector_z[i]]
        for i in range(len(rotated_vector_x))
    ]
    return rotated_vector


@ray.remote(num_cpus=1)
def render_flux_pulsar_body(func, points):
    vals = [func(point=point) for point in points]
    return vals


@ray.remote(num_cpus=1)
def run_gen_data_method(func, **kwargs):
    func(**kwargs)


#: Accesory functions
def distribute_arguments_list(num_cpus: int, list_of_args: list):    
    len_of_list = len(list_of_args)
    args_per_cpu = np.ceil(len_of_list / num_cpus)
    start_index = np.arange(0, len_of_list, args_per_cpu, dtype=np.int32)
    stop_index = np.arange(args_per_cpu, len_of_list, args_per_cpu, dtype=np.int32)
    stop_index = np.array(
        [
            stop_index[id] if id < len(stop_index) else len_of_list
            for id, x in enumerate(start_index)
        ],
        dtype=np.int32,
    )    
    distributed_arg = [
        list_of_args[start:end:1] for start, end in zip(start_index, stop_index)
    ]
    return distributed_arg


def flatten_distributed_returns(distributed_result: list):
    return [x for xs in distributed_result for x in xs]
