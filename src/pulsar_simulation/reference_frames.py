import numpy as np
from pyquaternion import Quaternion

""" Contains methods to deal with reference frame transformation calculations
"""


def calculate_geodesic_distance(
    point_0: list[float, float, float], point_1: tuple[float, float, float]
):
    """Calculates distance  along the sphere from point_0 to point_1 in cartesian coordinate system on a unit sphere.

    Args:
        point_0 (list[float,float,float]): unit directional vector in cartesian system
        point_1 (list[float,float,float]): unit directional vector in cartesian system
    """
    point_0_norm = np.linalg.norm(point_0)
    point_1_norm = np.linalg.norm(point_1)
    point_0 = (x / point_0_norm for x in point_0)
    point_1 = (x / point_1_norm for x in point_1)
    dot_product = sum(x * y for (x, y) in zip(point_0, point_1))
    distance = np.arccos(dot_product)
    return distance


def rotate_reference_frame(
    frame_0_basis: list[list], axis_of_rotation: list, rotation_angle: float
):
    """This method returns the transformed basis vectors after rotation

    Args:
        frame_0_basis (list[list]): list of old basis vectors
        axis_of_rotation (tuple): axis along which rotation takes place
        rotation_angle (float): angle in radians

    Returns:
        list: list of new corresponding basis vects
    """
    #print(f'DEBUG: <rotate_reference_frame> axis_of_rotation {axis_of_rotation} ')
    quaternion_rotator = Quaternion(axis=axis_of_rotation, angle=rotation_angle)
    frame_1_basis = list(quaternion_rotator.rotate(x) for x in frame_0_basis)
    #print(f'DEBUG: <rotate_reference_frame> frame_1_basis {frame_1_basis} ')
    return frame_1_basis

def polar2cart(r:float, theta:float, phi:float):
    return [
         r * np.sin(theta) * np.cos(phi),
         r * np.sin(theta) * np.sin(phi),
         r * np.cos(theta)
    ]


if __name__ == "__main__":
    print((calculate_geodesic_distance(point_0=(1, 0, 0), point_1=(0, 1, 0))))
    print(
        (
            rotate_reference_frame(
                frame_0_basis=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
                axis_of_rotation=(1, 0, 0),
                rotation_angle=np.pi / 4,
            )
        )
    )
