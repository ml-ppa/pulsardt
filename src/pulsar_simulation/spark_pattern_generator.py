import numpy as np
from pyquaternion import Quaternion

from .defines import *
from .reference_frames import polar2cart, rotate_reference_frame


class SparkSpectralModel:
    """This class deals with determining the spectral model of individual sparks"""

    def __init__(self, model: int = spark_constants.SPARK_SPECTRAL_MODEL, **kwargs):
        """This class deals with determining the spectral model of individual sparks

        Args:
            model (int, optional): The user can specify here which model to use specified by an integer. Defaults to spark_constants.SPARK_SPECTRAL_MODEL.
        """
        self.__model = model
        self.__dict__.update(**kwargs)

    def __call__(self, freq: float, center_freq: float):
        """This returns the relative flux based on the spectral model selected at a given frequency

        Args:
            freq (float): frequency in GHz
            center_freq (float): Center frequency or mid frequency used to define the spark in GHz

        Returns:
            float: Relative amplitude at that given frequency
        """
        if self.__model == 0:
            try:
                spectral_index = self.spectral_index
            except:
                spectral_index = -0.47
            try:
                b = self.b
            except:
                b = 0.21

            relative_amplitude = self.model_0(
                freq=freq, spectral_index=spectral_index, b=b, center_freq=center_freq
            )

        elif self.__model == 1:
            try:
                curvature_parameter = self.curvature_parameter
            except:
                curvature_parameter = 0
            try:
                spectral_index = self.spectral_index
            except:
                spectral_index = -0.47

            relative_amplitude = self.model_1(
                freq=freq,
                spectral_index=spectral_index,
                center_freq=center_freq,
                curvature_parameter=curvature_parameter,
            )

        else:
            print(f"WARNING: model_{self.__model} not defined")
            relative_amplitude = np.nan
        return relative_amplitude

    def model_0(
        self,
        freq: float,
        spectral_index: float = -0.47,
        b: float = 0.21,
        center_freq: float = 1.3,
    ):
        """Spectral model describing simple power law

        Args:
            freq (float): frequency in GHz
            spectral_index (float, optional): spectral index of the power lay or also denoted as alpha. Defaults to -0.47.
            b (float, optional): constant. Defaults to 0.21.
            center_freq (float, optional): Center frequency or mid frequency used to define the spark in GHz. Defaults to 1.3.

        Returns:
            float: Relative amplitude at that given frequency
        """
        relative_amplitude = b * (freq / center_freq) ** (spectral_index)
        return relative_amplitude

    def model_1(
        self,
        freq: float,
        curvature_parameter: float = 0,
        c: float = 1,
        spectral_index: float = -0.47,
        center_freq: float = 1.3,
    ):
        """Spectral model describing Log parabolic spectrum or LPS

        Args:
            freq (float): frequency in GHz
            curvature_parameter (float, optional): curvature parameter. Defaults to 0.
            c (float, optional): offset. Defaults to 1.
            spectral_index (float, optional): spectral index for a = 0. Defaults to -0.47.
            center_freq (float, optional): Center frequency or mid frequency used to define the spark in GHz. Defaults to 1.3.

        Returns:
            float: Relative amplitude at that given frequency
        """
        x = np.log10(freq / center_freq)
        c = 1
        realtive_amplitude = np.max(
            [0, 10 ** (curvature_parameter * x**2 + spectral_index * x + c)]
        )
        return realtive_amplitude


class SparkPatternGenerator:
    """This class creates a function for calculating radio flux from a spark based on its position in the pulsars rotational frame of reference"""

    def __init__(
        self,
        spark_id: int,
        spark_dimension: float = 0.1,
        spark_center: list[float, float] = [
            10,
            0,
        ],  # considering the spark rotation axis as the reference
        spark_rotation_axis_polar_att0: list[float, float] = [45, 0],
        drift_vel: float = -10 / 360,
        drift_phase: float = 0,
        spark_mid_freq: float = 125,
        spark_flux_power: float = 1,
    ):
        self.spark_id = spark_id
        self.__spark_center = spark_center
        self.__spark_dimension = spark_dimension
        self.__spark_rotation_axis_polar_att0 = spark_rotation_axis_polar_att0
        self.__drift_vel = drift_vel
        self.__drift_phase = drift_phase
        self.__mid_freq = spark_mid_freq
        self.__flux_power = spark_flux_power

    def __call__(
        self,
        pulsar_rot_frame_basis: list[list],
        pulsar_rot_phase: float,
        position: list[float, float, float],
    ):
        return self.apply_physics_model(
            pulsar_rot_frame_basis=pulsar_rot_frame_basis,
            pulsar_rot_phase=pulsar_rot_phase,
            position=position,
        )

    def get_spark_freq(self):
        """This method returns the mid-frequency of the spark object

        Returns:
            float: mid frquency of the spark
        """
        return self.__mid_freq

    def calculate_spark_center_att(
        self, pulsar_rot_frame_basis: list[list], pulsar_rot_phase: float
    ):
        """This method calculates the new spark position as it rotates with a drift velocity together with the pulsar's rotation

        Args:
            pulsar_rot_frame_basis (list[list]): the basis vectors aligned with the rotation axis of the pulsar
            pulsar_rot_phase (float): rotation phase in degrees

        Returns:
            list: a vector representing the new position of the spark
        """
        mag_frame_basis = self.calculate_magframe_basis_att(
            pulsar_rot_frame_basis=pulsar_rot_frame_basis,
            pulsar_rot_phase=pulsar_rot_phase,
        )
        quaternion_rotator = Quaternion(
            axis=mag_frame_basis[2],
            angle=self.__drift_vel * pulsar_rot_phase / 180 * np.pi
            + self.__drift_phase / 180 * np.pi,
        )
        spark_center_cartesian_att0_in_mag_frame = polar2cart(
            r=1,
            theta=self.__spark_center[0] / 180 * np.pi,
            phi=self.__spark_center[1] / 180 * np.pi,
        )
        spark_center_cartesian_att0_x = sum(
            spark_center_cartesian_att0_in_mag_frame[x] * mag_frame_basis[x][0]
            for x in [0, 1, 2]
        )
        spark_center_cartesian_att0_y = sum(
            spark_center_cartesian_att0_in_mag_frame[x] * mag_frame_basis[x][1]
            for x in [0, 1, 2]
        )
        spark_center_cartesian_att0_z = sum(
            spark_center_cartesian_att0_in_mag_frame[x] * mag_frame_basis[x][2]
            for x in [0, 1, 2]
        )
        spark_center_cartesian_att0 = [
            spark_center_cartesian_att0_x,
            spark_center_cartesian_att0_y,
            spark_center_cartesian_att0_z,
        ]
        spark_center_cartesian_att = quaternion_rotator.rotate(
            spark_center_cartesian_att0
        )
        return spark_center_cartesian_att

    def calculate_magframe_basis_att(
        self, pulsar_rot_frame_basis: list[list], pulsar_rot_phase: float
    ):
        """This method calculates the basis vectors aligned to the magnetic axis of the pulsar

        Args:
            pulsar_rot_frame_basis (list[list]): the basis vectors aligned with the rotation axis of the pulsar
            pulsar_rot_phase (float): rotation phase in degrees

        Returns:
            list[list]: magnetic frame basis vectors
        """
        rot_frame_z_axis = pulsar_rot_frame_basis[2]
        rot_frame_x_axis, rot_frame_y_axis = self.find_perpendicular_basis_vectors(
            vector=rot_frame_z_axis
        )

        mag_frame_basis_att0 = rotate_reference_frame(
            frame_0_basis=[rot_frame_x_axis, rot_frame_y_axis, rot_frame_z_axis],
            axis_of_rotation=rot_frame_y_axis,
            rotation_angle=self.__spark_rotation_axis_polar_att0[0] / 180 * np.pi,
        )

        mag_frame_basis_att0 = rotate_reference_frame(
            frame_0_basis=mag_frame_basis_att0,
            axis_of_rotation=rot_frame_z_axis,
            rotation_angle=self.__spark_rotation_axis_polar_att0[1] / 180 * np.pi,
        )

        mag_frame_basis = rotate_reference_frame(
            frame_0_basis=mag_frame_basis_att0,
            axis_of_rotation=rot_frame_z_axis,
            rotation_angle=pulsar_rot_phase * np.pi / 180,
        )
        return mag_frame_basis

    def apply_physics_model(
        self,
        pulsar_rot_frame_basis: list[list],
        pulsar_rot_phase: float,
        position: list[float, float, float],
    ):
        """This method should return spark amplitude at time t and position pos in the pulsar rotation frame

        Args:
            pulsar_rot_frame_basis (list[list]): the basis vectors aligned with the rotation axis of the pulsar
            pulsar_rot_phase (float): rotation phase in degrees
            position (list[float, float, float]): flux emitted by the spark at this position

        Returns:
            float: flux
        """
        spark_center_cartesian_att = self.calculate_spark_center_att(
            pulsar_rot_frame_basis=pulsar_rot_frame_basis,
            pulsar_rot_phase=pulsar_rot_phase,
        )
        position_mag = np.linalg.norm(position)
        position = [x / position_mag for x in position]
        radian_distance = np.arccos(np.dot(a=position, b=spark_center_cartesian_att))
        if (
            radian_distance
            >= spark_constants.SPARK_RADIUS_THRESH_FACTOR * self.__spark_dimension
        ):
            amplitude = 0.0
        else:
            amplitude = self.__flux_power * np.exp(
                -(radian_distance**2) / (2 * self.__spark_dimension**2)
            )
        return amplitude

    def get_spark_flux(self):
        return self.__flux_power
    def get_drift_vel(self):
        return self.__drift_vel


    @classmethod
    def create_patterened_spark_pattern(
        cls,
        num_sparks: list[int],
        conal_latitudes: list[float],
        spark_rotation_axis_polar_att0: list[float, float] = [45, 0],
        drift_vel: float = -10 / 360,
        spark_dimension: float = 0.3,
        spark_mid_freqs: list[float] = None,
        spark_flux_powers: list[float] = None,
    ):
        """This method generates a list of sparks arranged in nested cones

        Args:
            num_sparks (list[int]): list of the number of sparks in each nested cone
            conal_latitudes (list[float]): corresponding conal latitudes in degrees
            spark_rotation_axis_polar_att0 (list[float, float], optional): The center point or the center axis of all the nested cones. Defaults to [45, 0].
            drift_vel (float, optional): drift velocity of the sparks in degrees travelled per degree of rotation. Defaults to -10/360.
            spark_dimension (float, optional): spark size in radians. Defaults to 0.3.
            spark_mid_freqs (list[float], optional): corresponding list of mid or center freqs of the nested cones. Defaults to None.
            spark_flux_powers (list[float], optional): list of flux per spark in a nested cone. Defaults to None.

        Returns:
            list[SparkPatternGenerator]: list of sparks arranged in nested cones
        """
        # spark_ids = [x for x in range(sum(num_sparks))]
        num_cones = len(num_sparks)
        spark_list = []
        spark_id_noter = 0
        for id, lat in enumerate(conal_latitudes):
            num_spark = num_sparks[id]
            if spark_mid_freqs:
                spark_freq = spark_mid_freqs[id] * 1.0
                spark_flux_power = spark_flux_powers[id] * 1.0
            else:
                spark_freq = 125.0
                spark_flux_power = 1.0
            spark_ids = np.arange(spark_id_noter, spark_id_noter + num_spark, 1)
            current_spark_list = [
                cls(
                    spark_id=spark_id,
                    spark_dimension=spark_dimension,
                    spark_center=[lat, 0],
                    spark_rotation_axis_polar_att0=spark_rotation_axis_polar_att0,
                    drift_vel=drift_vel,
                    drift_phase=phase,
                    spark_mid_freq=spark_freq,
                    spark_flux_power=spark_flux_power,
                )
                for spark_id, phase in zip(
                    spark_ids, np.arange(0, 360, 360 / num_spark)
                )
            ]
            spark_id_noter = spark_id_noter + num_spark
            [spark_list.append(x) for x in current_spark_list]
        return spark_list

    @staticmethod
    def find_perpendicular_basis_vectors(vector: list):
        """Returns basis vector perpendicular to the given vector. The basis directions are calculated by differentiating the phi and theta in polar coordinate system

        Args:
            vector (list): a 3d vector

        Returns:
            [list,list]: returns two basis vectors orthogonal to the given vector in the perpendicula plane
        """
        a, b, c = vector
        ortho_vector_parametric = lambda parameter: [
            -b * np.cos(parameter)
            - a * c / ((a**2 + b**2) ** (0.5)) * np.sin(parameter),
            a * np.cos(parameter)
            - b * c / ((a**2 + b**2) ** (0.5)) * np.sin(parameter),
            (a**2 + b**2) ** (0.5) * np.sin(parameter),
        ]
        if a == 0 and b == 0:
            ortho_vector_x = [1, 0, 0]
            ortho_vector_y = [0, 1, 0]
        else:
            ortho_vector_x = ortho_vector_parametric(parameter=0)
            ortho_vector_y = ortho_vector_parametric(parameter=np.pi / 2)
        return ortho_vector_x, ortho_vector_y
