import os
import numpy as np
import ray
from .defines import *
from .information_packet_formats import Payload
from .reference_frames import rotate_reference_frame
from .spark_pattern_generator import (
    SparkPatternGenerator,
    SparkSpectralModel,
)
from .remote_functions import (
    distribute_arguments_list,
    flatten_distributed_returns,
    render_flux_pulsar_body,
)

num_cpus = os.cpu_count()


class PulsarAnimator:
    """This class animates a pulsar rotator as viewed from Earth. The frame of reference is the Earth's celestial coordinate system"""

    def __init__(self, rot_axis: list, mag_axis_tilt: float, period: float = 33):
        """Initializes a pulsar animator with pulsar state vectors at time = 0

        Args:
            rot_axis (list): 3d list representing the rotation axis of pulsar in the earths frame of reference (except translation)
            mag_axis_tilt (float): representing magnetic axis tilt away from rot axis in degrees
            period (float): time in ms for one complete rotation of the pulsar
        """
        self.__rot_axis = rot_axis
        self.__mag_axis_tilt = mag_axis_tilt
        self.period = period
        
        self.mag_frame_basis_att0 = None
        
        self.rot_frame_basis = None
        self.mag_frame_basis = None
        self.spark_genlist = []
        self.spark_spectrum_model_list = []

    def add_spark(
        self,
        spark_gen: SparkPatternGenerator,
        spark_spectrum_model: SparkSpectralModel = SparkSpectralModel(),
    ):
        """This method adds a spark at the pulsar surface

        Args:
            spark_gen (SparkPatternGenerator): A digital twin of a spark
            spark_spectrum_model (SparkSpectralModel, optional): The spectral model of the spark. Defaults to SparkSpectralModel().
        """
        self.spark_genlist.append(spark_gen)
        self.spark_spectrum_model_list.append(spark_spectrum_model)
        

    def __call__(self, rot_phase: float):
        """This function needs to be called to return the magnetic frame basis vectors at a given rotational phase

        Args:
            rot_phase (float): rotation phase in degrees

        Returns:
            mag_frame_basis (list[list]): returns magnetic frame basis vectors
        """
        self.update_pulsar_state_vectors(rot_phase=rot_phase)
        mag_frame_basis = self.mag_frame_basis
        return mag_frame_basis

    def update_pulsar_state_vectors(self, rot_phase: float):
        """This function calculates the state vectors of the pulsar

        Args:
            rot_phase (float): rotation phase in degrees
        """
        rot_frame_z_axis = self.__rot_axis
        rot_frame_x_axis, rot_frame_y_axis = self.find_perpendicular_basis_vectors(
            vector=rot_frame_z_axis
        )

        mag_frame_basis_att0 = rotate_reference_frame(
            frame_0_basis=[rot_frame_x_axis, rot_frame_y_axis, rot_frame_z_axis],
            axis_of_rotation=rot_frame_y_axis,
            rotation_angle=self.__mag_axis_tilt / 180 * np.pi,
        )
        self.rot_frame_basis = [rot_frame_x_axis, rot_frame_y_axis, rot_frame_z_axis]
        self.mag_frame_basis_att0 = mag_frame_basis_att0
        self.mag_frame_basis = rotate_reference_frame(
            frame_0_basis=mag_frame_basis_att0,
            axis_of_rotation=rot_frame_z_axis,
            rotation_angle=rot_phase * np.pi / 180,
        )

    @staticmethod
    def find_perpendicular_basis_vectors(vector: list):
        """Returns basis vector perpendicular to the given vector. The basis directions are calculated by differentiating the phi and theta in polar coordinate system

        Args:
            vector (list): a 3d vector

        Returns:
            [list,list]: returns two basis vectors orthogonal to the given vector in the perpendicular plane
        """
        a, b, c = vector
        ortho_vector_parametric = lambda parameter: [
            -b * np.cos(parameter)
            - a * c / ((a**2 + b**2) ** (0.5)) * np.sin(parameter),
            a * np.cos(parameter)
            - b * c / ((a**2 + b**2) ** (0.5)) * np.sin(parameter),
            (a**2 + b**2) ** (0.5) * np.sin(parameter),
        ]
        if a == 0 and b == 0:
            ortho_vector_x = [1, 0, 0]
            ortho_vector_y = [0, 1, 0]
        else:
            ortho_vector_x = ortho_vector_parametric(parameter=0)
            ortho_vector_y = ortho_vector_parametric(parameter=np.pi / 2)
        return ortho_vector_x, ortho_vector_y

    def render_radiation_hotspots_on_surface(
        self, rot_phase: float, points: list[list]
    ):
        """This method renders the radio intensity profile throughout the sphere describing the pulsar surface

        Args:
            rot_phase (float): rotation phase of the pulsar in degrees
            points (list[list]): interesting points on the surface to render the radio intensity

        Returns:
            list[float]: rendered radio intensities
        """
        
        pulsar_rot_frame_basis = self.rot_frame_basis
        spark_pattern = self.spark_genlist
        flux_lambda = lambda point: self.calculate_flux_amplitude_at_refframe_position(
            spark_pattern=spark_pattern,
            pulsar_rot_frame_basis=pulsar_rot_frame_basis,
            rot_phase=rot_phase,
            cartesian_coor=point,
        )

        points_distributed = distribute_arguments_list(
            num_cpus=num_cpus, list_of_args=points
        )

        func_handle = ray.put(flux_lambda)
        flux_distributed = ray.get(
            [
                render_flux_pulsar_body.options(scheduling_strategy="SPREAD").remote(
                    func=func_handle, points=x
                )
                for x in points_distributed
            ]
        )
        rendered_flux_mags = flatten_distributed_returns(
            distributed_result=flux_distributed
        )

        return rendered_flux_mags

    def generate_radio_packet(self, rot_phase: float, direction: list[float]):
        """This method generates a tuple lists of radio flux values and the corresponding center frequencies of the sparks

        Args:
            rot_phase (float): The rotation phase of the pulsar in degrees
            direction (list[float]): a vector specifying the line of sight towards Earth

        Returns:
            tuple[list,list]: radio packet as tuple containing list of flux values and corresponding center frequencies of the sparks
        """
        
        pulsar_rot_frame_basis = self.rot_frame_basis
        spark_patterns = self.spark_genlist
        radio_packet = self.calculate_radio_packet_at_direction(
            spark_patterns=spark_patterns,
            direction=direction,
            pulsar_rot_frame_basis=pulsar_rot_frame_basis,
            rot_phase=rot_phase,
        )
        return radio_packet

    @staticmethod
    def calculate_flux_amplitude_at_refframe_position(
        spark_pattern: list[SparkPatternGenerator],
        cartesian_coor: list,
        pulsar_rot_frame_basis: list[list],
        rot_phase: float,
    ):
        """This method calculates the flux at the specified point

        Args:
            spark_pattern (list[SparkPatternGenerator]): List of radio hot spots or sparks
            cartesian_coor (list): vector representing the cartesian coordinate
            pulsar_rot_frame_basis (list[list]): pulsars static frame basis vectors where the pulsar's rotation axis is alligned to the z axis
            rot_phase (float): rotation phase in degrees

        Returns:
            float: flux magnitude
        """
        flux_list = (
            spark(
                position=cartesian_coor,
                pulsar_rot_frame_basis=pulsar_rot_frame_basis,
                pulsar_rot_phase=rot_phase,
            )
            for spark in spark_pattern
        )
        return sum(flux_list)

    @staticmethod
    def calculate_radio_packet_at_direction(
        spark_patterns: list[SparkPatternGenerator],
        
        direction: list[float],
        pulsar_rot_frame_basis: list[list],
        rot_phase: float,
    ):
        """This method calculates a radio packet at a particular direction

        Args:
            spark_patterns (list[SparkPatternGenerator]): List of radio hot spots or sparks
            direction (list[float]): vector representing the direction at which the packet is emitted
            pulsar_rot_frame_basis (list[list]): pulsars static frame basis vectors where the pulsar's rotation axis is alligned to the z axis
            rot_phase (float): rotation phase in degrees

        Returns:
            (list,list): radio packet as tuple containing list of flux values and corresponding center frequencies of the sparks
        """
        mid_freq_list = np.array([spark.get_spark_freq() for spark in spark_patterns])
        flux_from_sparks_list = np.array(
            [
                spark(
                    position=direction,
                    pulsar_rot_frame_basis=pulsar_rot_frame_basis,
                    pulsar_rot_phase=rot_phase,
                )
                for spark in spark_patterns
            ]
        )
        return flux_from_sparks_list.tolist(), mid_freq_list.tolist()

    def generate_radio_packet_stream(
        self, rot_phases: list[float], direction: list[float]
    ):
        """This method calculates radio packets at a particular direction for the list of rotation phases

        Args:
            rot_phases (list[float]): list of rotation phases in degrees
            direction (list[float]): vector representing the direction at which the packet is emitted


        Returns:
            Payload: payload object representing the radi data stream
        """
        rot_phases = [rf * 1.0 for rf in rot_phases]
        freq_list = [spark.get_spark_freq() for spark in self.spark_genlist]
        payload_obj = Payload(freqs=freq_list)
        _ = [
            payload_obj.add_flux(
                radio_packet=self.generate_radio_packet(
                    rot_phase=x, direction=direction
                )
            )
            for x in rot_phases
        ]

        payload_obj.assign_rot_phases(rot_phases=rot_phases)
        return payload_obj


if __name__ == "__main__":
    pulsar_obj = PulsarAnimator(rot_axis=[0, 1, 1], mag_axis_tilt=45)
    pulsar_obj(rot_phase=0)
