import os
import time
import numpy as np
import pyvista as pv
import ipywidgets as widgets
num_cpus=os.cpu_count()
numTaskpCPU = 1



from .pulsar_animator import PulsarAnimator
from .reference_frames import polar2cart
from .spark_pattern_generator import SparkPatternGenerator
from .remote_functions import distribute_arguments_list,flatten_distributed_returns,render_flux_pulsar_body


class PulsarPlotter():
    """ This class plots the pulsar rotator as viewed from Earth. The frame of reference is the Earth's celestial coordinate system
    """    
    def __init__(self,pulsar_animator:PulsarAnimator,res:float=50):
        self.__window_name = "ML-PPA DigitalTwin"        
        self.__p = pv.Plotter()
        self.__pulsaranimator = pulsar_animator
        self.__ref_mesh = pv.Sphere(radius=1,theta_resolution=res,phi_resolution=res)
        self.__p.add_mesh(self.__ref_mesh,show_edges=True,opacity=0.2,name='ref_sphere',style='wireframe')
        self.__rot_phase = 0
        self.__switch_mag_frame = False
        self.__switch_spark = False
        self.__switch_render_flux = False
        

    def __call__(self):
        self.create_scene()
        self.plot_mag_frame_state_vectors(switch=True)
        self.plot_spark_centers(switch=True)
        self.__p.add_slider_widget(
                 callback=lambda value: self.update_rot_phase(rot_phase=float(value)),
                 rng=[0,360],
                 value=.5,
                 title="Rot Phase",
                 pointa=(.8, 0.1),
                 pointb=(1, 0.1),                 
                 style='modern',
             )
        self.__p.add_checkbox_button_widget(
                callback=lambda value: self.update_to_plot_magframe(switch=value),                
                value=False,
                 size=40,
                border_size=1,
                color_on='green',
                color_off='grey',
                position=(50, 50)
                )
        self.__p.add_checkbox_button_widget(
                callback=lambda value: self.update_to_plot_sparks(switch=value),                
                value=False,
                 size=40,
                border_size=1,
                color_on='green',
                color_off='grey',
                position=(150, 50)
                )
        self.__p.add_checkbox_button_widget(
                callback=lambda value: self.update_to_render_flux_on_surface(switch=value),                
                value=False,
                 size=40,
                border_size=1,
                color_on='green',
                color_off='grey',
                position=(250, 50)
                )
        
        self.__p.show(title='Pulsar Plotter')
        
        

    def create_scene(self):
        self.__pulsaranimator(rot_phase=self.__rot_phase)
        #starting_mesh = pv.Sphere(radius=2,theta_resolution=20,phi_resolution=20)
        rot_frame_basis = self.__pulsaranimator.rot_frame_basis
        rot_frame_x_axis = pv.Arrow(start=[0,0,0],direction=rot_frame_basis[0])
        rot_frame_y_axis = pv.Arrow(start=[0,0,0],direction=rot_frame_basis[1])
        rot_frame_z_axis = pv.Arrow(start=[0,0,0],direction=rot_frame_basis[2],scale=2)
        rot_frame_z_axis_reverse = pv.Arrow(start=[0,0,0],direction=[-x for x in rot_frame_basis[2]],scale=2)
        colors = ['blue','green','red'] 
        self.__p.add_mesh(rot_frame_x_axis,color=colors[0],name='rot_frame_x_handle',opacity=0.2)
        self.__p.add_mesh(rot_frame_y_axis,color=colors[1],name='rot_frame_y_handle',opacity=0.2)
        self.__p.add_mesh(rot_frame_z_axis,color=colors[2],name='rot_frame_z_handle',opacity=0.2)
        self.__p.add_mesh(rot_frame_z_axis_reverse,color=colors[2],name='rot_frame_zinverse_handle',opacity=0.2)

    def plot_mag_frame_state_vectors(self,switch:bool):
        if switch:
            mag_frame_vectors = self.__pulsaranimator(rot_phase=self.__rot_phase)
            colors = ['blue','green','red']  
            mesh_x_axis = pv.Arrow(start=[0,0,0],direction=mag_frame_vectors[0])
            mesh_y_axis = pv.Arrow(start=[0,0,0],direction=mag_frame_vectors[1])
            mesh_z_axis = pv.Arrow(start=[0,0,0],direction=mag_frame_vectors[2])
            actor_x_handle=self.__p.add_mesh(mesh_x_axis,color=colors[0],name='x_handle')
            actor_y_handle=self.__p.add_mesh(mesh_y_axis,color=colors[1],name='y_handle')
            actor_z_handle=self.__p.add_mesh(mesh_z_axis,color=colors[2],name='z_handle')
        else:
            mag_frame_vectors = self.__pulsaranimator(rot_phase=self.__rot_phase)
            colors = ['blue','green','red']  
            mesh_x_axis = pv.Arrow(start=[0,0,0],direction=mag_frame_vectors[0])
            mesh_y_axis = pv.Arrow(start=[0,0,0],direction=mag_frame_vectors[1])
            mesh_z_axis = pv.Arrow(start=[0,0,0],direction=mag_frame_vectors[2])
            actor_x_handle=self.__p.add_mesh(mesh_x_axis,color=colors[0],name='x_handle',opacity=0.2)
            actor_y_handle=self.__p.add_mesh(mesh_y_axis,color=colors[1],name='y_handle',opacity=0.2)
            actor_z_handle=self.__p.add_mesh(mesh_z_axis,color=colors[2],name='z_handle',opacity=0.2)
    def update_to_plot_magframe(self,switch:bool):
        self.__switch_mag_frame = switch

    def plot_spark_centers(self,switch:bool,id_to_mark:int=10):
        if switch:
            if self.__pulsaranimator.spark_genlist:
                for id,spark_gen in enumerate(self.__pulsaranimator.spark_genlist):
                    spark_gen_center = spark_gen.calculate_spark_center_att(pulsar_rot_frame_basis=self.__pulsaranimator.rot_frame_basis,pulsar_rot_phase=self.__rot_phase)
                    mesh_spark_center = pv.Arrow(start=[0,0,0],direction=spark_gen_center)
                    if id==id_to_mark:
                        color = 'blue'
                    else:
                        color = 'yellow'
                    self.__p.add_mesh(mesh_spark_center,color=color,name='spark_no'+str(spark_gen.spark_id))
        else:
            if self.__pulsaranimator.spark_genlist:
                for spark_gen in (self.__pulsaranimator.spark_genlist):
                    spark_gen_center = spark_gen.calculate_spark_center_att(pulsar_rot_frame_basis=self.__pulsaranimator.rot_frame_basis,pulsar_rot_phase=self.__rot_phase)
                    mesh_spark_center = pv.Arrow(start=[0,0,0],direction=spark_gen_center)
                    self.__p.add_mesh(mesh_spark_center,color='yellow',name='spark_no'+str(spark_gen.spark_id),opacity=0.2)
    def update_to_plot_sparks(self,switch:bool):
        self.__switch_spark = switch

    def update_rot_phase(self,rot_phase:float):
        self.__rot_phase = rot_phase
        self.plot_spark_centers(switch=self.__switch_spark)
        self.plot_mag_frame_state_vectors(switch=self.__switch_mag_frame)
        self.plot_rendered_pulsar()

    def render_radiation_hotspots_on_surface(self):
        rot_phase = self.__rot_phase
        rendered_flux_mags = self.__pulsaranimator.render_radiation_hotspots_on_surface(rot_phase=self.__rot_phase,points=self.__ref_mesh.points)
        return rendered_flux_mags
    
    def plot_rendered_pulsar(self):
        if self.__switch_render_flux:
            rendered_flux_mags = self.render_radiation_hotspots_on_surface()
            self.__p.add_mesh(self.__ref_mesh,show_edges=False,opacity=1,name='ref_sphere',scalars = rendered_flux_mags)
        else:
            self.__p.add_mesh(self.__ref_mesh,show_edges=True,opacity=0.2,name='ref_sphere',style='wireframe')
        
    def update_to_render_flux_on_surface(self,switch:bool):
        self.__switch_render_flux = switch

    @staticmethod
    def calculate_flux_amplitude_at_refframe_position(spark_pattern:list[SparkPatternGenerator],cartesian_coor:list,pulsar_rot_frame_basis:list[list],rot_phase:float):        
        flux_list = (spark(position=cartesian_coor,pulsar_rot_frame_basis=pulsar_rot_frame_basis,pulsar_rot_phase=rot_phase) for spark in spark_pattern) 
        return sum(flux_list)    



    



    
           
        




    def plot_flux_profile_at_position(self,rot_phase:float,position_unit_sphere_polar:list[float,float]):        
        cartesian_position_pulsar_frame = polar2cart(r=1,theta=position_unit_sphere_polar[0]/180*np.pi,phi=position_unit_sphere_polar[1]/180*np.pi)
        flux=0
        if self.__pulsaranimator.spark_genlist:
            for spark_gen in (self.__pulsaranimator.spark_genlist):
                flux += spark_gen(pulsar_rot_frame_basis=self.__pulsaranimator.rot_frame_basis,pulsar_rot_phase=rot_phase,position=cartesian_position_pulsar_frame)
        return flux
    
    def plot_subflux_profile_at_position(self,rot_phase:float,position_unit_sphere_polar:list[float,float]):
        cartesian_position_pulsar_frame = polar2cart(r=1,theta=position_unit_sphere_polar[0]/180*np.pi,phi=position_unit_sphere_polar[1]/180*np.pi)
        flux=0
        num_sub_profiles = len(self.__pulsaranimator.spark_genlist)        
        if self.__pulsaranimator.spark_genlist:
            flux_noter = [x(pulsar_rot_frame_basis=self.__pulsaranimator.rot_frame_basis,pulsar_rot_phase=rot_phase,position=cartesian_position_pulsar_frame) for x in self.__pulsaranimator.spark_genlist]
            
        return flux_noter
    
    def plot_flux_profile_at_position_all_rotphase(self,position_unit_sphere_polar:list[float,float]):
        rot_phases= [x for x in range(0,360*2,1)]
        return rot_phases,[self.plot_flux_profile_at_position(rot_phase=x,position_unit_sphere_polar=position_unit_sphere_polar) for x in rot_phases]
    
    def plot_subflux_profiles_at_position_all_rotphase(self,position_unit_sphere_polar:list[float,float]):
        rot_phases= [x for x in range(180,360+180,1)]
        num_sub_profiles = len(self.__pulsaranimator.spark_genlist)
        flux_noter = np.empty(shape=(num_sub_profiles,len(rot_phases)))
        for id,x in enumerate(rot_phases):
            flux_noter[:,id] = self.plot_subflux_profile_at_position(rot_phase=x,position_unit_sphere_polar=position_unit_sphere_polar)
        return [rot_phases,flux_noter]
    

    def plot_flux_profile_at_position_all_rotphase_at_multiple_rotcycles(self,position_unit_sphere_polar:list[float,float],rot_cycle_start:list[float]):
        rot_phases= [x for x in range(0,360*1,2)]
        rot_phase_mat_lambda = lambda start_phase,rot_phase : start_phase + rot_phase
        rot_phases_mat = np.array([[rot_phase_mat_lambda(start_phase=s_p,rot_phase=r_d) for r_d in rot_phases] for s_p in rot_cycle_start]) 
        plot_flux_profile_at_position_vectorized = np.vectorize(self.plot_flux_profile_at_position,excluded=['position_unit_sphere_polar'])
        flux = plot_flux_profile_at_position_vectorized(rot_phase=rot_phases_mat,position_unit_sphere_polar=position_unit_sphere_polar)
        return rot_phases_mat,flux
        
        
            
        