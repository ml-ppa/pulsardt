import json
class Params:    
    def __init__(self,InputConstantFunc):
        self.__InputConstantFunc = InputConstantFunc

    def __call__(self,**kwargs):
        self.__dict__.update(self.__InputConstantFunc(**kwargs))
        return self
    
    def __repr__(self):
        d = self.__dict__
        dict_to_print = {k: d[k] for k in d.keys() if k!='_Params__InputConstantFunc'}  
        return str(dict_to_print)     

    def write_to_json_file(self,filename:str='..\\params\constants.json'):
        self.__InputConstantFunc = self.__InputConstantFunc.__name__
        with open(filename, "w") as outfile:
            json_object = json.dumps(self.__dict__,indent=4) 
            outfile.write(json_object)
        
@Params
def SetUnitDefinations(TIME='ms',FREQUENCY='GHz',DISTANCE='parsec'):
    return {'TIME':TIME,'FREQUENCY':FREQUENCY,'DISTANCE':DISTANCE}

@Params
def SetSparkConstants(SPARK_RADIUS_THRESH_FACTOR = 5,SPARK_SPECTRAL_MODEL = 0,SPECTRAL_INDEX = -0.47):    
    return {'SPARK_RADIUS_THRESH_FACTOR':SPARK_RADIUS_THRESH_FACTOR,
            'SPARK_SPECTRAL_MODEL':SPARK_SPECTRAL_MODEL,
            'SPECTRAL_INDEX':SPECTRAL_INDEX}

@Params
def SetAvgSparkParams(num_cones:int = 2,
                        avg_spark_dimension:float=7,
                        avg_spark_per_cone_length:float=.05,
                        avg_spark_pattern_center_tilt:float=45,
                        avg_drift_vel:float=1):    
    return {'num_cones':num_cones,
            'avg_spark_dimension':avg_spark_dimension,
            'avg_spark_per_cone_length':avg_spark_per_cone_length,
            'avg_spark_pattern_center_tilt':avg_spark_pattern_center_tilt,
            'avg_spark_dimension':avg_spark_dimension,
            'avg_drift_vel':avg_drift_vel
            }

@Params
def SetAvgISMParams(avg_dm_homogeneous:float=2.643,
                    std_dm:float=0.2,
                    avg_scintillation_index_homo:float=1,
                    std_scintillation_index:float=100):    
    return {'avg_dm_homogeneous':avg_dm_homogeneous,
            'std_dm':std_dm,
            'avg_scintillation_index_homo':avg_scintillation_index_homo,
            'std_scintillation_index':std_scintillation_index,            
            }

unit_defines = SetUnitDefinations()
spark_constants = SetSparkConstants()
avg_spark_pattern_params = SetAvgSparkParams()
avg_ism_params = SetAvgISMParams()







