import numpy as np
from numpy import convolve
import math as mt
from .information_packet_formats import Payload


class InterstellarMedium:
    """This class deals with the properties and modulation of the radio signals through ISM"""

    def __init__(
        self,
        dm_homogeneous: float,
        scintillation_index_homo: float,
        std_dm=None,
        std_scintillation_index=None,
    ):
        self.dm_homogeneous = dm_homogeneous
        self.scintillation_index_homo = scintillation_index_homo
        self.std_dm = std_dm
        self.std_scintillation_index = std_scintillation_index
        pass

    def randomize_props(self):
        """Randomizes properties"""
        if self.std_dm:
            self.dm_homogeneous = np.max(
                [0, np.random.normal(loc=self.dm_homogeneous, scale=self.std_dm)]
            )
        else:
            print(
                "WARNING: InterstellarMedium.randomize_props called with std_dm == None"
            )
            
        if self.std_scintillation_index:
            self.scintillation_index_homo = np.max(
                [
                    0,
                    np.random.normal(
                        loc=self.scintillation_index_homo,
                        scale=self.std_scintillation_index,
                    ),
                ]
            )
        else:
            print(
                "WARNING: InterstellarMedium.randomize_props called with scintillation_index_homo == None"
            )
            

    def propagate_through(self, payload: Payload, signal_time_bin: float,average_over_period:bool=False):
        """This method propagates the payload along the ISM and returns dispersed payload

        Args:
            payload (Payload): payload near the pulsar surface to be propagated
            signal_time_bin (float): time bin in millisecond
            average_over_period (bool): If True, the drift of subpulse is gone

        Returns:
            Payload: payload after propagation
        """
        rot_phases = np.array(payload.rot_phases)
        freq_channels = payload.freqs
        flux_freq_channels = np.array(payload.dataframe)
        max_freq = np.max(freq_channels)        
        rot_phase_shifts = np.array(
            [
                self.calc_disperse_phase_shift(
                    freq=f_c, freq_ref=max_freq, signal_time_bin=signal_time_bin
                )
                for f_c in freq_channels
            ]
        )
        if average_over_period:
            dispersed_flux = np.array(
                [
                    np.interp(rot_phases, rot_phases + rps, flux_freq_channels[:, id],left=0,period=360)
                    for id, rps in enumerate(rot_phase_shifts)
                ]
            )
        else:
            dispersed_flux = np.array(
                [
                    np.interp(rot_phases, rot_phases + rps, flux_freq_channels[:, id],left=0)
                    for id, rps in enumerate(rot_phase_shifts)
                ]
            )
        payload_propagated = Payload(freqs=np.flip(freq_channels).tolist())
        payload_propagated.dataframe = np.flipud(dispersed_flux).T.tolist()
        payload_propagated.assign_rot_phases(rot_phases=payload.rot_phases)
        return payload_propagated

    def calc_disperse_phase_shift(
        self, freq: float, freq_ref: float = 1, signal_time_bin: float = 1
    ):
        """Calculates phase shift due to dispersion

        Args:
            freq (float): frequency of the channel in GHz
            freq_ref (float, optional): the reference frequency in GHz. Defaults to 1.
            signal_time_bin (float, optional): time bin in millisecond. Defaults to 1.

        Returns:
            float: phase shift in degrees
        """
        phase_shift = (
            0
            + 4.15
            * self.dm_homogeneous
            / 1
            * (1 / freq ** (2) - 1 / freq_ref ** (2))
            / signal_time_bin
        )
        return phase_shift

    def calc_scintillation_curve(
        self, freq: float, len_of_signal: int, signal_time_bin: float = 1
    ):
        """calculates the scintillation curve

        Args:
            freq (float): frequency of the channel in GHz
            len_of_signal (int): number of time points in the signal
            signal_time_bin (float, optional): time bin in millisecond. Defaults to 1.

        Returns:
            list[float | int]: scintillation curve
        """
        scintillation_index_homo = self.scintillation_index_homo
        if freq == 0:
            freq = 0.0001
        tau = scintillation_index_homo * freq ** (-4.4)
        scintillation_curve = [
            mt.exp(-((x - len_of_signal / 2) * signal_time_bin) / tau)
            if (x - len_of_signal / 2) >= 0
            else 0
            for x in range(len_of_signal)
        ]
        return scintillation_curve

    def scintillate_signal_simple(
        self, signal: list, freq: float, signal_time_bin: float = 1
    ):
        """Scintillate the signal

        Args:
            signal (list): datastream for the particular frequency channel
            freq (float): frequency of the channel in GHz
            signal_time_bin (float, optional): time bin in millisecond. Defaults to 1.

        Returns:
            list[float]: scintillated signal
        """
        scintillation_index_homo = self.scintillation_index_homo
        tau = scintillation_index_homo * freq ** (-4.4)
        scintillation_curve = [
            mt.exp(-((x - len(signal) / 2) * signal_time_bin) / tau)
            if (x - len(signal) / 2) >= 0
            else 0
            for x, _ in enumerate(signal)
        ]
        modulated_signal = convolve(signal, scintillation_curve, mode="same") / sum(
            scintillation_curve
        )
        return modulated_signal.tolist()
