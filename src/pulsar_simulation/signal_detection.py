import numpy as np
from .information_packet_formats import Payload


class antenna:
    """Digital twin of Antenna"""

    def __init__(self,sensitivity:float=0.1,
                 prob_bbrfi:float=1,
                prob_nbrfi:float=1
                ):
        self.freq_channels = []
        self.sensitivity = sensitivity
        self.prob_nbrfi = prob_nbrfi
        self.prob_bbrfi = prob_bbrfi

    def __call__(self, payload: Payload):
        return self.randomize_antenna_output_method(payload=payload)

    def randomize_antenna_output_method(self, payload: Payload):
        """returns detected flux in voltage along with added noise

        Args:
            payload (Payload): Payload falling on the antenna detector

        Returns:
            (NDArray[floating[Any]],NDArray[floating[Any]]): noisy voltage dataframe and payload dataframe with NBRFI + BBRFI
        """
        self.extract_payload_info(payload=payload)
        effected_rot_phase_by_bbrfi = self.rot_phases[
            int(np.random.random() * len(self.rot_phases))
        ]
        effected_freq_channel_by_nbrfi = self.freq_channels[
            int(np.random.random() * len(self.freq_channels))
        ]
        total_noise,description = self.gen_terresterial_noise(
            effected_freq_channel_by_nbrfi=effected_freq_channel_by_nbrfi,
            nbrfi_bandwidth=0.01,
            effected_rot_phase_by_bbrfi=effected_rot_phase_by_bbrfi,
            bbrfi_duration=10,
            noise_amplitude_range=[3, 10],            
        )
        pulsar_count = self.is_pulsar_present()
        total_flux = self.dataframe + total_noise.T
        total_noisy_output = self.calculateAntennaOutputVoltage(
            received_flux=total_flux
        )
        description.update({'Pulsar':pulsar_count})
        
        return total_noisy_output, total_flux,description

    def is_pulsar_present(self):
        dataframe = np.array(self.dataframe)
        
        if np.max(dataframe.flatten())<(self.sensitivity):
            self.dataframe = np.zeros(np.array(self.dataframe).shape)
            pulsar_count = 0
            
        else:
            pulsar_count = 1
        #print(f'max of dataframe normed {np.max(dataframe.flatten()) } and sensitivity {self.sensitivity}')
        return pulsar_count

    def gen_terresterial_noise(
        self,
        effected_freq_channel_by_nbrfi: float,
        nbrfi_bandwidth: float,
        effected_rot_phase_by_bbrfi: float,
        bbrfi_duration: float,
        noise_amplitude_range: list[float],
        
    ):
        """Generates dataframe of NBRFI and BBRFI

        Args:
            effected_freq_channel_by_nbrfi (float): effected freq channel in GHz
            nbrfi_bandwidth (float): spread of the noise in GHz
            effected_rot_phase_by_bbrfi (float): rot phase or time point at which BBRFI occurs
            bbrfi_duration (float): spread in rot phase
            noise_amplitude_range (list[float]): amplitude range of noise
            prob_nbrfi (float, optional): probability of occuring NBRFI. Defaults to 0.5.
            prob_bbrfi (float, optional): probability of occuring BBRFI. Defaults to 0.5.

        Returns:
            NDArray[floating[Any]]: dataframe of noise
        """
        prob_nbrfi = self.prob_nbrfi
        prob_bbrfi = self.prob_bbrfi
        nbrfi_amplitude = np.min(noise_amplitude_range) + (
            np.random.random()
            * abs(noise_amplitude_range[1] - noise_amplitude_range[0])
        )
        bbrfi_amplitude = np.min(noise_amplitude_range) + (
            np.random.random()
            * abs(noise_amplitude_range[1] - noise_amplitude_range[0])
        )
        if np.random.random() < prob_nbrfi:
            nbrfi_noise = self.gen_noise_NBRFI(
                effected_freq_channel=effected_freq_channel_by_nbrfi,
                nbrfi_amplitude=nbrfi_amplitude,
                spread_in_channels=nbrfi_bandwidth,
            )
            nbrfi_count = 1
        else:
            nbrfi_noise = np.zeros(np.array(self.dataframe).shape).T
            nbrfi_count = 0

        if np.random.random() < prob_bbrfi:
            bbrfi_noise = self.gen_noise_BBRFI(
                effected_rot_phase=effected_rot_phase_by_bbrfi,
                bbrfi_amplitude=bbrfi_amplitude,
                spread_in_phase=bbrfi_duration,
            )
            bbrfi_count = 1
        else:
            bbrfi_noise = np.zeros(np.array(self.dataframe).shape).T
            bbrfi_count = 0
        total_noise = nbrfi_noise + bbrfi_noise
        description = {'NBRFI':nbrfi_count,'BBRFI':bbrfi_count}
        return total_noise,description

    def extract_payload_info(self, payload: Payload):
        """extracts payload props

        Args:
            payload (Payload): payload falling on antenna
        """
        self.freq_channels = payload.freqs
        self.rot_phases = payload.rot_phases
        self.dataframe = payload.dataframe

    def gen_noise_NBRFI(
        self,
        effected_freq_channel: float,
        nbrfi_amplitude: float = 1.0,
        spread_in_channels: float = 0.5,
    ):
        """Generates NBRFI containing dataframe

        Args:
            effected_freq_channel (float): effected freq channel in GHz
            nbrfi_amplitude (float, optional): noise amplitude. Defaults to 1.0.
            spread_in_channels (float, optional): spread of the noise in GHz. Defaults to 0.5.

        Returns:
            NDArray[floating[Any]]: dataframe of noise
        """
        effected_phase = self.rot_phases[int(np.random.random() * len(self.rot_phases))]
        noise_val_func = (
            lambda current_freq_channel, current_phase: nbrfi_amplitude
            * np.exp(
                -((-effected_freq_channel + current_freq_channel) ** 2)
                / 2
                / spread_in_channels**2
            )
            * np.exp(-((-effected_phase + current_phase) ** 2) / 2 / 720**2)
        )
        
        freq_channel_mat = np.matmul(
            np.array([self.freq_channels]).T, np.ones((1, len(self.rot_phases)))
        )
        phase_mat = np.matmul(
            np.array([self.rot_phases]).T, np.ones((1, len(self.freq_channels)))
        ).T
        nbrfi_noise = noise_val_func(freq_channel_mat, phase_mat)
        return nbrfi_noise

    def gen_noise_BBRFI(
        self,
        effected_rot_phase: float,
        bbrfi_amplitude: float = 1.0,
        spread_in_phase: float = 10,
    ):
        """Generates BBRFI containing dataframe

        Args:
            effected_rot_phase (float): rot phase or time point at which BBRFI occurs
            bbrfi_amplitude (float, optional): noise amplitude. Defaults to 1.0.
            spread_in_phase (float, optional): spread in rot phase. Defaults to 10.

        Returns:
            NDArray[floating[Any]]: dataframe of noise
        """
        effected_freq_channel = self.freq_channels[
            int(np.random.random() * len(self.freq_channels))
        ]
        noise_val_func = (
            lambda current_freq_channel, current_phase: bbrfi_amplitude
            * np.exp(
                -((-effected_freq_channel + current_freq_channel) ** 2) / 2 / 10**2
            )
            * np.exp(
                -((-effected_rot_phase + current_phase) ** 2) / 2 / spread_in_phase**2
            )
        )
        
        freq_channel_mat = np.matmul(
            np.array([self.freq_channels]).T, np.ones((1, len(self.rot_phases)))
        )
        phase_mat = np.matmul(
            np.array([self.rot_phases]).T, np.ones((1, len(self.freq_channels)))
        ).T
        bbrfi_noise = noise_val_func(freq_channel_mat, phase_mat)
        return bbrfi_noise

    def antennaTemperature(self, receivedFlux: float):
        """Converts flux to temperature

        Args:
            receivedFlux (float): flux falling on antenna

        Returns:
            float: temperature
        """
        conversion_func = lambda flux: flux
        antennaTemp = conversion_func(flux=receivedFlux)
        return antennaTemp

    def calculateAntennaOutputVoltage(self, received_flux: float):
        """Converts antenna temperature to voltage

        Args:
            received_flux (float): flux falling on antenna

        Returns:
            float: voltage
        """
        T_S = 1
        bandwidth = 1
        acquisitiontime = 1
        T_A = self.antennaTemperature(receivedFlux=received_flux)
        voltage_func_std = lambda T_A, T_S, bandwidth, acquisitiontime: (T_A + T_S) / (
            bandwidth * acquisitiontime
        ) ** (0.5)
        voltage_func_mean = lambda T_A, T_S: (T_A + T_S)
        voltage_output_func = lambda voltage_mean, voltage_std: np.random.normal(
            loc=voltage_mean, scale=voltage_std
        )
        voltage_mean = voltage_func_mean(T_A=T_A, T_S=T_S)
        voltage_std = voltage_func_std(
            T_A=T_A, T_S=T_S, bandwidth=bandwidth, acquisitiontime=acquisitiontime
        )
        voltage_output = voltage_output_func(
            voltage_mean=voltage_mean, voltage_std=voltage_std
        )
        return voltage_output
