# PulsarDT

<img src="docs/graphics/Asset 1.png" alt= “PackageLogo” width=20% height=20%>

Welcome to Pulsar Digital Twin python package Version (0.2). This package in its current form provides a digital twin/animation of a Pulsar. Using this package a user can design a pulsar source with a customized radio emission pattern or sparks. In addition specific physics such as spectral emission profile associated with each sparks can also be modelled. The sparks can also have their own drift velocity. At its present form a method is provided to make sparks arranged in nested cone pattern. 
A pipeline is also provided to generate random frequency-time graphs containing Pulsar+NBRFI+BBRFI dataframes which will be used to train neural network models to detect pulsars from radio data streams generated from radio-telescopes like MeerKAT. 

If you want to run and test the code, we recommend starting with the  [Tutorial Project](https://gitlab.com/ml-ppa/tutorial_project), where you can use a containerized solution for convenience. 

You can also compile and run it locally using virtual environments; therefore, follow the steps explained in the next section. 

### Introduction

The package contains these fundamental modular blocks for simulating pulsar signals:
* Pulsar animation package: [PulsarAnimator](src/pulsar_simulation/pulsar_animator.py)
* Spark pattern generator package: [SparkPatternGenerator](src/pulsar_simulation/spark_pattern_generator.py)
* Interstellar medium: [InterstellarMedium](src/pulsar_simulation/radio_packet_propagation.py)
* Antenna: [antenna](src/pulsar_simulation/signal_detection.py)

### What's new in version 0.2

In this version, we introduce several key updates compared to version 0.1:
* **Spark Pattern Generator**: A new module has been added to create custom spark patterns, inspired by the model proposed by [J.A. Gil and M. Sendyk](https://iopscience.iop.org/article/10.1086/309394/pdf)
* **Optimized Data Generation Pipeline**: The data generation pipeline has been modularized and optimized using Ray, with added functionality to save outputs at various stages for enhanced data reproducibility.
* **Advanced Visualization Tool**: An upgraded visualization tool has been integrated to plot pulsar states using PyVista, providing more detailed and interactive representations.
* **Automated Testing** Ensuring quality control for further iterations.
* **Installable via pip** This tool can easily be installed through the package provided in our registry. Check the [ML-PPA Package Registry](https://gitlab.com/ml-ppa/python-pkgs/-/packages) for instructions.

## Setup to run it locally
Following these steps will enable you to install all necessary dependencies as well as build and install PularDT in an virtual environment. The project has been *tried and tested on Ubuntu 22.04 (x86_64)*, if you are using any other operating system inform yourself how to install the necessary dependencies. From this point you can run the Jupyter Notebooks provided in the [examples](examples/) folder and test the package.

For Ubuntu systems, please install the following dependencies:

```
sudo apt update
sudo apt install git python3.10-venv build-essential python3-dev
```
The package can be clonned from the gitlab repository as:
```
git clone https://gitlab.com/ml-ppa/pulsardt.git
cd pulsardt
```
We recommend using virtual environment for running this package. This will help organize dependencies and avoid conflics with packages you have installed globally on your computer. You can create a virtual environment with the following command:
```
python3 -m venv <venv_name>
``` 
followed by activating the virtual environment as:
```
source <venv_name>/bin/activate
```
If you have activated the virtual environment, you should see the name <venv_name> at the beginning of the command prompt in your terminal. The required packages or dependencies then can be installed in the virtual environment as:
```
pip install -r requirements.txt
```


### Implementation

Detailed steps of the pipeline to generate freq-time dispersion graphs is explained in this [pipeline_gen_freq_time.ipynb](examples/pipeline_gen_freq_time.ipynb) 

- Parameters of the simulation are defined in [defines.py](src/pulsar_simulation/defines.py) and can be accessed and modified by calling this functions

    ```python 
    import src.pulsar_simulation.defines as defines 
    unit_defines = defines.SetUnitDefinations(); print(unit_defines)
    spark_constants = SetSparkConstants(); print(spark_constants)
    avg_spark_pattern_params = SetAvgSparkParams(); print(avg_spark_pattern_params)
    avg_ism_params = SetAvgISMParams(); print(avg_ism_params)
    ```

- A random freq-time graph can be called using the method
    ```python
    from src.pulsar_simulation.pulsar_animator import PulsarAnimator 
    from src.pulsar_simulation.generate_data_pipeline import GenData

    pulsar_animator = PulsarAnimator(rot_axis=[0,0,1],mag_axis_tilt=45)
    gendata_obj = GenData(pulsar_animator=pulsar_animator)
    total_noisy_output,total_flux=gendata_obj(tag='test_0',
                                            rot_phases=np.arange(0,720*1,1).tolist(),
                                            freq_channels=np.arange(0.5,1.6,0.001),
                                            param_folder='./params/runtime/',
                                            payload_folder='./params/payloads/')
    ```
- The Pulsar animator along with spark configuration can be visualized using 
    ```python
    from src.pulsar_simulation.pulsar_plotter import PulsarPlotter
    pulsar_plotter = PulsarPlotter(pulsar_animator=pulsar_animator)
    pulsar_plotter()
    ```
    Below is a animation created using the plotter:
    
    <img src="docs/graphics/state_vector_multi_perspective.gif" alt= “PackageLogo” width=60% height=60%>


- The generated data is saved in file and can be accessed by
    ```python
    from src.pulsar_simulation.information_packet_formats import Payload
    payload_obj_flux = Payload.read_payload_from_jsonfile('./params/payloads/test_0_payload_flux.json')
    payload_obj_detected = Payload.read_payload_from_jsonfile('./params/payloads/test_0_payload_detected.json')
    payload_obj_flux.plot();payload_obj_detected.plot()
    ```
    payload_obj_detected             |  payload_obj_flux
    :-------------------------:|:-------------------------:
    <img src="docs/graphics/noisy_dataframe.png" alt= “PackageLogo” width=60% height=60%> |  <img src="docs/graphics/flux_dataframe.png" alt= “PackageLogo” width=60% height=60%>

- To run several such methods in parallel the following code below can be used marked with unique tags
    ```python
    #: Please run this part from main.py (or from <any>.py file in the same directory as main.py)
    from src.pulsar_simulation.generate_data_pipeline import run_gen_data_in_parallel
    run_gen_data_in_parallel(gendata_obj=gendata_obj,
                            tag_list=['test_1','test_2','test_3','test_4','test_5','test_6'],
                            param_folder='./params/runtime/',
                            payload_folder='./params/payloads/'
                            )
    ```

- Below is the time graph to execute n tasks in parallel in the dev's machine. The graph fitted in green is 
    $$ y = T_s + T_p/x + (x-1)T_c $$ 

    and in red

    $$ y = T_s + T_p/x $$

    where x=num_cpus is number of CPUs and y=T_n is the time for executing (in seconds). 

    The dashed curves in the diagram show a 2-dimensional and a 3-dimensional fit to the time for the parallel execution of the framework. They indicate that the sequential part (T_s) of the task is much smaller than the parallelizable part (T_p). At most, there is little communication between the CPUs, as the time T_c is quite short.

    <img src="docs/graphics/speed_test_num_task_10.png" alt= “PackageLogo” width=30% height=30%> 


    


## Authors and acknowledgment
**Authors**: Tanumoy Saha (HTW Berlin)   
**Acknowledgment**: We would like to acknowledge PUNCH4NFDI and InterTwin consortium for the funding and the members of TA5 for their valuable support 

## License
Free to use GNU GENERAL PUBLIC LICENSE Version 3.

## Project status
Developing (Version: 0.2).
