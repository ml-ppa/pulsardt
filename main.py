import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
plt.style.use('ggplot')

from src.pulsar_simulation.pulsar_plotter import *
from src.pulsar_simulation.defines import *
from src.pulsar_simulation.pulsar_animator import PulsarAnimator
from src.pulsar_simulation.spark_pattern_generator import SparkPatternGenerator,SparkSpectralModel
from src.pulsar_simulation.generate_data_pipeline import GenData
from src.pulsar_simulation.generate_data_pipeline import run_gen_data_in_parallel

unit_defines = SetUnitDefinations(); print(unit_defines)
spark_constants = SetSparkConstants(); print(spark_constants)
avg_spark_pattern_params = SetAvgSparkParams(); print(avg_spark_pattern_params)
avg_ism_params = SetAvgISMParams(); print(avg_ism_params)


#: Instantiate pulsar animator and gendata 
pulsar_animator = PulsarAnimator(rot_axis=[0,0,1],mag_axis_tilt=45)
gendata_obj = GenData(pulsar_animator=pulsar_animator)
total_noisy_output,total_flux=gendata_obj(tag='test_0',
                                          rot_phases=np.arange(0,720*1,1).tolist(),
                                          freq_channels=np.arange(0.5,1.6,0.001),
                                          param_folder='./params/runtime/',
                                          payload_folder='./params/payloads/'
                                          )

#: Run simulations in parallel id with tags
run_gen_data_in_parallel(gendata_obj=gendata_obj,tag_list=['test_1','test_2','test_3','test_4','test_5','test_6'],payload_folder='./params/payloads/',param_folder='./params/runtime/')

#: speed test
num_cpus = int(os.cpu_count()/2)
print(f'Total number of cpus in this machine is {num_cpus}')
T_n = np.zeros(num_cpus)
n_tasks = np.arange(1,num_cpus+1,1)
for i in range(num_cpus):
    num_task_current = 4
    tags_curr = ['tp_'+str(i)+str(x) for x in range(num_task_current)]    
    T_n[i] = run_gen_data_in_parallel(gendata_obj=gendata_obj,tag_list=tags_curr,num_cpus=i+1,payload_folder='./params/payloads/')    
    print(f'MESSAGE: Tasks {num_task_current} ran with {i+1} cpus took: {T_n[i]}')

#: Plot the results
curve_fit_func = lambda x,T_p,T_s,T_c: T_s + T_p/x + (x-1)*T_c
curve_fit_func_no_comm = lambda x,T_p,T_s: T_s + T_p/x 
popt, pcov = curve_fit(curve_fit_func, n_tasks, T_n)
popt_ncomm, pcov_ncomm = curve_fit(curve_fit_func_no_comm, n_tasks, T_n)

fig_scaling, ax = plt.subplots(1,1)
plt.plot(n_tasks, T_n, 'b-*', label='Time to execute')
plt.plot(n_tasks, curve_fit_func(n_tasks, *popt), 'g--',
         label='fit: T_p=%5.3f secs, T_s=%5.3f secs, T_c=%5.3f secs' % tuple(popt))
plt.plot(n_tasks, curve_fit_func_no_comm(n_tasks, *popt_ncomm), 'r--',
         label='fit: T_p=%5.3f secs, T_s=%5.3f secs' % tuple(popt_ncomm))
plt.xlabel('num_cpus')
plt.ylabel('T_n (secs)')
plt.xlim(1,num_cpus)
plt.xticks(np.arange(1,num_cpus+1,1))
plt.legend()
plt.show()
